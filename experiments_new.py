import xml.etree.ElementTree
import subprocess
import math
import os
import pylab
from csv import reader
import numpy as np

curr_dir = os.getcwd()
global results_to_write

grid_path = '/Results_Grid/'
uniform_path = '/Results_Uniform/'
measure_index = { "RANDOM" : 1, "DEGREE" : 2, "CLOSENESS" : 3}
save_file_name = "experiment_results.csv"
num_trials = 30

#Variables for grid distribution
initial_row_length = 2
max_row_length = 17

#Variables for uniform distribution
argosTree = xml.etree.ElementTree.parse('experiments/static_interaction.argos')
root = argosTree.getroot()
fbElement = root.find('arena').find('distribute').find('entity').find('foot-bot')
rab_range = float(fbElement.attrib['rab_range'])
initial_area_length = math.sqrt( ( ((rab_range - 0.1)*3)**2)/2 )
initial_num_robots = 5
max_num_robots = 105
uniform_increment_size = 5
area_per_robot = (initial_area_length**2) / initial_num_robots
spawn_area_min = '0,0,0'

intervals,random_seed,results_to_write = 0,0,0

#Remove any old experimental results
def clear_old_files(dir_path = '/'):
	for file_name in os.listdir(os.getcwd()+dir_path):
		file_prefix, file_extension = os.path.splitext(file_name)
		if file_extension == '.csv' or file_extension == '.png':
			os.remove(curr_dir+dir_path+file_name)

#Move results
def move_old_files(dir_path):
	for file_name in os.listdir(os.getcwd()):
		file_prefix, file_extension = os.path.splitext(file_name)
		if file_extension == '.csv' or file_extension == '.png':
			if not os.path.exists(curr_dir+dir_path):
				os.makedirs(curr_dir+dir_path)
			os.rename(curr_dir+'/'+file_name,curr_dir+dir_path+file_name)

#Setup experiment variables
def setup_experiment(distribution):
	if distribution == 'grid':
		intervals = range(initial_row_length,max_row_length)
		random_seed = 124
		results_to_write = [ [str(x**2),"-1","-1","-1"] for x in intervals ]
	elif distribution == 'uniform':
		intervals = range(initial_num_robots,max_num_robots,uniform_increment_size)
		random_seed = 124
		results_to_write = [ [str(x),"-1","-1","-1"] for x in intervals ]
	else:
		intervals = None
		random_seed = None
		results_to_write = None
		print "MAJOR ERROR: Specify experiment distribution"
	
	return intervals,random_seed,results_to_write
		
#Conduct experiment
def run_experiment(distribution,packet_loss_prob):
	f= open("comm_config2.txt","r")
	configs = f.read()
	f.close()
	configs = configs.strip().split("\n")
	config_list = {}
	
	for i in range(len(configs)):
		item = configs[i].split(',')
		if not config_list.has_key(item[0]):
			config_list[item[0]] = {}
		config_list[item[0]][item[1]] = item[2]
	
	for trial in range(num_trials):
		for interval in intervals:
			num_robots_sqrt = str(interval)
			new_area_length = math.sqrt(area_per_robot * interval)
			num_robots = -1
			if distribution == 'grid':
				num_robots = str(interval**2)
			elif distribution == 'uniform':
				num_robots = str(interval)
				
			argosTree = xml.etree.ElementTree.parse('experiments/static_interaction.argos')
			root = argosTree.getroot()
			
			#Adjust random seed
			experimentElement = root.find('framework').find('experiment')
			val = random_seed + trial
			experimentElement.set('random_seed',str(val))
			
			#Modify number of robots in .argos file
			quantityElement = root.find('arena').find('distribute').find('entity')
			quantityElement.set('quantity',num_robots)
			
			#Set communication range
			fbElement = root.find('arena').find('distribute').find('entity').find('foot-bot')
			fbElement.set('rab_range',config_list[str(val)][num_robots])
			
			#Set probability of packet loss
			sensorElement = root.find('controllers').find('interaction_controller').find('sensors').find('range_and_bearing')
			sensorElement.set('packet_drop_prob',packet_loss_prob)
				
			with open('experiments/human_interaction.argos','w') as f:
				f.write(xml.etree.ElementTree.tostring(root))
				
			process = subprocess.Popen(['argos3 -c experiments/human_interaction.argos'],shell=True)
			process.wait()

#Create an accumulated results file
def accumulate_results(distribution):
	for file_name in os.listdir(os.getcwd()):
		file_prefix, file_extension = os.path.splitext(file_name)
		name_arr = file_prefix.split('_')
		if file_extension == '.csv':
			f = open(file_name)
			file_contents = f.read()
			f.close()
			results_list = file_contents.strip().split('\n')
			new_results_list1 = [int(x.split(',')[0]) for x in results_list]
			new_results_list2 = [int(x.split(',')[1]) for x in results_list]
			results_list = new_results_list1
			results_list_elec = new_results_list2
			mean = np.mean(results_list)
			std = np.std(results_list)
			mean_elec = np.mean(results_list_elec)
			std_elec = np.std(results_list_elec)
			index = measure_index[name_arr[0]]
			if distribution == 'grid':
				results_to_write[intervals.index(int(math.sqrt(int(name_arr[1]))))][index] = str(mean)+','+str(std)+','+str(mean_elec)+','+str(std_elec)
			elif distribution == 'uniform':
				results_to_write[intervals.index(int(name_arr[1]))][index] = str(mean)+','+str(std)+','+str(mean_elec)+','+str(std_elec)

	accumulated_file = open(save_file_name,'w+')
	accumulated_file.write("Number of Robots,Random Mean,Random Std Dev,Random Mean (with election),Random Std Dev (with election),Degree Mean,Degree Std Dev,Degree Mean (with election),Degree Std Dev (with election),Closeness Mean,Closeness Std Dev,Closeness Mean (with election),Closeness Std Dev (with election)\n")
	for row in results_to_write:
		for c in range(len(row)):
			col = row[c]
			string_to_write = str(col)
			accumulated_file.write(string_to_write)
			if c != len(row)-1:
				accumulated_file.write(",")
			else:
				accumulated_file.write("\n")
	accumulated_file.close()

#Generate Graph of results
def generate_graph(distribution,with_error=False,with_election=False):
	with open(save_file_name, 'r') as f:
		data = list(reader(f))

	x_axis = [int(x[0]) for x in data[1:]]
	random_mean = [ float(x[1]) for x in data[1:]]
	random_std = [ float(x[2]) for x in data[1:]]
	random_mean_elec = [ float(x[3]) for x in data[1:]]
	random_std_elec = [ float(x[4]) for x in data[1:]]
	
	degree_mean = [ float(x[5]) for x in data[1:]]
	degree_std = [ float(x[6]) for x in data[1:]]
	degree_mean_elec = [ float(x[7]) for x in data[1:]]
	degree_std_elec = [ float(x[8]) for x in data[1:]]
	
	closeness_mean = [ float(x[9]) for x in data[1:]]
	closeness_std = [ float(x[10]) for x in data[1:]]
	closeness_mean_elec = [ float(x[11]) for x in data[1:]]
	closeness_std_elec = [ float(x[12]) for x in data[1:]]

	pylab.xlim(0,350)
	pylab.ylim([0,100])

	pylab.xlabel(data[0][0])
	pylab.ylabel('Convergence Time')
	if distribution == 'grid':
		pylab.title('Convergence Time in a Lattice Distribution')
	elif distribution == 'uniform':
		pylab.title('Convergence Time in a Uniform Distribution')

	if with_error and with_election:
		pylab.errorbar(x_axis, random_mean_elec, yerr=random_std_elec, label='Random', fmt='b')
		pylab.errorbar(x_axis, degree_mean_elec, yerr=degree_std_elec, label='Degree', fmt='r')
		pylab.errorbar(x_axis, closeness_mean_elec, yerr=closeness_std_elec, label='Closeness', fmt='g')
		pylab.legend(loc='upper right')
		pylab.savefig('results_with_error_and_election.png')
		pylab.clf()
	elif with_error and not with_election:
		pylab.errorbar(x_axis, random_mean, yerr=random_std, label='Random', fmt='b')
		pylab.errorbar(x_axis, degree_mean, yerr=degree_std, label='Degree', fmt='r')
		pylab.errorbar(x_axis, closeness_mean, yerr=closeness_std, label='Closeness', fmt='g')
		pylab.legend(loc='upper right')
		pylab.savefig('results_with_error.png')
		pylab.clf()
	elif not with_error and with_election:
		pylab.plot(x_axis, random_mean_elec, 'bo')
		pylab.plot(x_axis, degree_mean_elec, 'ro')
		pylab.plot(x_axis, closeness_mean_elec, 'go')
		pylab.plot(x_axis, random_mean_elec, '--b', label='Random')
		pylab.plot(x_axis, degree_mean_elec, '--r', label='Degree')
		pylab.plot(x_axis, closeness_mean_elec, '--g', label='Closeness')
		pylab.legend(loc='upper right')
		pylab.savefig('results_with_election.png')
		pylab.clf()
	else:
		pylab.plot(x_axis, random_mean, 'bo')
		pylab.plot(x_axis, degree_mean, 'ro')
		pylab.plot(x_axis, closeness_mean, 'go')
		pylab.plot(x_axis, random_mean, '--b', label='Random')
		pylab.plot(x_axis, degree_mean, '--r', label='Degree')
		pylab.plot(x_axis, closeness_mean, '--g', label='Closeness')
		pylab.legend(loc='upper right')
		pylab.savefig('results.png')
		pylab.clf()

for p in [0.05,0.1,0.2]:
	clear_old_files()
	experiment_distribution = 'uniform'
	intervals,random_seed,results_to_write = setup_experiment(experiment_distribution)
	run_experiment(experiment_distribution,str(p))
	#accumulate_results(experiment_distribution)
	#generate_graph(experiment_distribution,with_error=True,with_election=True)
	#generate_graph(experiment_distribution,with_error=False,with_election=True)
	#generate_graph(experiment_distribution,with_error=False,with_election=False)
	move_old_files(uniform_path+str("/")+str(p)+str("/"))




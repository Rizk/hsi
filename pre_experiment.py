#Script for recording appropriate communication ranges for each quantity/seed configuration
import xml.etree.ElementTree
import subprocess
import math
import os

accumulated_file = open("configurations.txt",'w+')
accumulated_file.close()

rangeFound = False

for seed in range(154,184):
	argosTree = xml.etree.ElementTree.parse('experiments/RCC_interaction.argos')
	root = argosTree.getroot()
	
	#Remove visualization
	visualizationElement = root.find('visualization')
	root.remove(visualizationElement)
	
	#Set seed
	experimentElement = root.find('framework').find('experiment')
	experimentElement.set('random_seed',str(seed))

	for quantity in range(5,105,5):
		#Set number of robots
		quantityElement = root.find('arena').find('distribute').find('entity')
		quantityElement.set('quantity',str(quantity))
			
		for comm in range(1,21):
			if not rangeFound:
				#Set communication range
				fbElement = root.find('arena').find('distribute').find('entity').find('foot-bot')
				fbElement.set('rab_range',str(comm))
				
				with open('experiments/human_interaction.argos','wb') as f:
					f.write(xml.etree.ElementTree.tostring(root))
						
				process = subprocess.Popen(['argos3 -c experiments/human_interaction.argos'],shell=True)
				process.wait()
				
				f = open("success.txt","r")
				file_contents = f.read().strip()
				f.close()
				
				print("File conents are" + file_contents)
				
				if file_contents == "success":
					accumulated_file = open("configurations.txt",'a')
					accumulated_file.write(str(seed)+","+str(quantity)+","+str(comm)+'\n')
					accumulated_file.close()
					rangeFound = True
		rangeFound = False

/*
 * AUTHOR: Mostafa Rizk <mostafa.rizk@monash.edu
 * Modified from code by Carlo Pinciroli <cpinciro@ulb.ac.be>
 *
 * A controller for controlling a swarm via a selected leader

 * To control a foot-bot, you need to Shift-Click on it in the OpenGL
 * visualization to select it.
 *
 * This controller is meant to be used with the XML file:
 *    experiments/interaction.argos
 */

#ifndef INTERACTION_H
#define INTERACTION_H

/*
 * Include some necessary headers.
 */
/* Definition of the CCI_Controller class. */
#include <argos3/core/control_interface/ci_controller.h>
/* Definition of the differential steering actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_differential_steering_actuator.h>
/* Definition of the LEDs actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_leds_actuator.h>
/* Definition of the range-and-bearing sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_sensor.h>
/* Definition of the range-and-bearing actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_actuator.h>
/* Definition of the positioning sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_positioning_sensor.h>
/* Definition of the omnidirectional camera sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_colored_blob_omnidirectional_camera_sensor.h>
/* Definition of the foot-bot light sensor */
#include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_light_sensor.h>
/* Definition of the foot-bot proximity sensor */
#include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_proximity_sensor.h>
/* Vector2 definitions */
#include <argos3/core/utility/math/vector2.h>
/* Definition of random number generator */
#include <argos3/core/utility/math/rng.h>

/* Include uint8 and the likes */
#include <stdint.h>
/* Include vector library */
#include <vector>

/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;

/*
 * A controller is simply an implementation of the CCI_Controller class.
 */
class CInteraction : public CCI_Controller {

public:

   /*
    * The following variables are used as parameters for
    * turning during navigation. You can set their value
    * in the <parameters> section of the XML configuration
    * file, under the
    * <controllers><interaction_controller><parameters><wheel_turning>
    * section.
    */
   struct SWheelTurningParams {
      /*
       * The turning mechanism.
       * The robot can be in three different turning states.
       */
      enum ETurningMechanism
      {
         NO_TURN = 0, // go straight
         SOFT_TURN,   // both wheels are turning forwards, but at different speeds
         HARD_TURN    // wheels are turning with opposite speeds
      } TurningMechanism;
      /*
       * Angular thresholds to change turning state.
       */
      CRadians HardTurnOnAngleThreshold;
      CRadians SoftTurnOnAngleThreshold;
      CRadians NoTurnAngleThreshold;
      /* Maximum wheel speed */
      Real MaxSpeed;

      void Init(TConfigurationNode& t_tree);
   };
   
   /*
    * The following variables are used as parameters for
    * flocking interaction. You can set their value
    * in the <parameters> section of the XML configuration
    * file, under the
    * <parameters><flocking>
    * section.
    */
   struct SFlockingInteractionParams {
      /* Target robot-robot distance in cm */
      Real TargetDistance;
      /* Gain of the Lennard-Jones potential */
      Real Gain;
      /* Exponent of the Lennard-Jones potential */
      Real Exponent;
 
      void Init(TConfigurationNode& t_node);
      Real GeneralizedLennardJones(Real f_distance);
   };
   
   struct SProximityParams {
		/* Maximum tolerance for the angle between
		 * the robot heading direction and
		 * the closest obstacle detected. */
		 CDegrees m_cAlpha;
		/* Maximum tolerance for the proximity reading between
		 * the robot and the closest obstacle.
		 * The proximity reading is 0 when nothing is detected
		 * and grows exponentially to 1 when the obstacle is
		 * touching the robot.
		 */
		 Real m_fDelta;
		 Real m_fWheelVelocity;
		 CRange<CRadians> m_cGoStraightAngleRange;
		 
		 void Init(TConfigurationNode& t_node);
   };
   
   struct SCentralityParams {
	   std::string centralityMeasure;
	   
	   void Init(TConfigurationNode& t_tree);
   };
   
   // Keeps track of time steps. Increments for each robot, so needs to be divided by total robots
	int timeCounter; 

	int MAX_NUM_ROBOTS;

	// Each robot generates a message and passes along other robot's messages
	// size = 48 bytes
	struct message {
		int id; 
		uint8_t TTL;
		uint8_t degree;
		float value;
		CVector2 goal;
		CVector2 swarmHeading;
		int senderID;
		int maxCentrality;
		int maxID;
		bool isTagged;
		float centralityChange;
	};

	int MESSAGE_SIZE;

	int CBUF_SIZE; 

	int TTL_VALUE;
	
	/* Pointer to be incremented when items are added to the buffer */
	int bufferPointer;
	
	/* Buffer to be populated with messages then forwarded */
	CByteArray cBuf;
   
	int originalCentralityValue;
	
	int oldCentralityValue;
	
	int positiveOscChange;
	
	int negativeOscChange;
	
	float regChange;
	
	float oscChange;
	
	int centralityValue;
	
	/* Highest centrality value in the swarm */
	int maxCentralityValue;
	
	/* ID of robot with highest centrality */
	int maxCentralityID;
	
	/* Value that is passed to neighboring robots */
	float valueToPass;
	
	/* Indicates whether the value a robot has remains the same or changes */
	bool valueIsStable;
	
	bool TACIT_LEADERSHIP_ENABLED;
	
	/* True if consensus has been achieved in the local neigborhood */
	bool localConsensusAchieved;
	
	/* To be incremented for every time step when there is no consensus */
	int noConsensusTimer;
	
	/* Random number generator */
	CRandom::CRNG* randomNumberGenerator;
	
	/* if consensusResetTimer passes the threshold then the maxCentralityValue and maxID are reset */
	int LEADER_RESELECTION_THRESHOLD;
	
	/* To be incremented at every time step and reset when the CONSENSUS_RESET_THRESHOLD is passed */
	int leaderReselectionTimer;
	
	/* Random number to be stored for random centrality and only changed when the swarm moves */
	int randomNumber;
	
	/* Method being used to select the leader */
	std::string leaderSelectionMethod;
	
	bool convergedOnLeader;
	
	int consensusCounter;
	
	int CONSENSUS_THRESHOLD;
	
	bool centralityStored;
	
	/* If a robot has a neighbor with the same maximum centrality but a different maxID, there is a conflict 
	 * that will prevent consensus. Thus this value is true 
	 */
	bool consensusConflict;
	
	/*
	 * When tacit leadership is enabled, individuals multiply the values they receive by this weight, then take the average.
	 * So long as the weight is less than 1, the swarm is guaranteed to converge on a value
	 */
	float TACIT_CONSENSUS_WEIGHT;
	
	CVector2 robotVelocity;
	
	CVector2 goalLocation;
	
	CVector2 swarmHeading;
	
	/* Tuning parameter for speed. Fraction to be multiplied by the maximum wheel speed */
	Real SPEED_FACTOR;
	
	/* Hysteresis */
	int hysteresisTimer;
	int HYSTERESIS_MAX;
	CVector2 lastPosition;
	

public:

   /* Class constructor. */
   CInteraction();

   /* Class destructor. */
   virtual ~CInteraction() {}

   /*
    * This function initializes the controller.
    * The 't_node' variable points to the <parameters> section in the XML file
    * in the <controllers><interaction_controller> section.
    */
   virtual void Init(TConfigurationNode& t_node);

   /*
    * This function is called once every time step.
    * The length of the time step is set in the XML file.
    */
   virtual void ControlStep();

   /*
    * This function resets the controller to its state right after the Init().
    * It is called when you press the reset button in the GUI.
    */
   virtual void Reset();

   /*
    * Called to cleanup what done by Init() when the experiment finishes.
    * In this example controller there is no need for clean anything up, so
    * the function could have been omitted. It's here just for completeness.
    */
   virtual void Destroy() {}

   /*
    * Sets the selected flag on this robot.
    * When selected, a robot follows the control vector.
    */
   void Select();

   /*
    * Unsets the selected flag on this robot.
    * When unselected, a robot stays still.
    */
   void Deselect();

   /*
    * Sets the control vector.
    */
   void SetControlVector(const CVector2& c_control);
   
   /*
    * Retrieves centrality for outside classes to use
    */
   int GetCentrality();
   
   /*
    * Assign a value to be propagated via messages
    * If the 'tagged' flag is true, then the robot does not change its value
    */
   void SendValue(float value,bool tagged);
   
   /*
    * Assign a goal location to be propagated via messages
    * If the 'tagged' flag is true, then the robot does not change its value
    */
   void SendGoal(CVector2 goal,bool tagged);
   
   /*
    * Return the value that has been received from neighbors
    */
   float GetValue();
   
   /* 
    * Get id of the current robot 
    */
    int GetRobotID();
   
	/*
     * Returns the id of the node currently considered the leader
     */
    int GetLeaderID();
    
    /*
     * Determines the method that will be used to select the leader
     */
    void SetLeaderSelectionMethod(std::string method);
    
    /*
     * Returns true if the robot has had the same leader for a certain number of time steps
     */
    bool ConvergedOnLeader();
    
    /*
     * Returns the ids of all of a robot's neighbors
     */
    std::vector<int> GetNeighbors();
    
    /*
     * Uses one of multiple metrics to approximate how far the leader's rank has dropped
     */
    float GetApproximateRankDrop();
    
    /*
     * Resets all rank drop approximation variables
     */
    void ResetApproximateRankDrop();
    
    /*
     * One metric for approximating the leader's rank drop
     */
    float GetRegularChange();
    
    /* Get difference in change metric of this robot with the average of its neighbors */
    float GetNeighborDifference();
    
    /*
     * Resets necessary variables for regular change
     */
    void ResetRegularChange();
    
    /*
     * One metric for approximating the leader's rank drop
     */
    float GetOscillatoryChange();
    
    /*
     * Resets necessary variables for oscillatory change
     */
    void ResetOscillatoryChange();
    
   /*
    * Calculates the degree centrality
    */
   int GetDegreeCentrality();
   
    /*
     * Get the position of the robot
     */
    CVector2 GetPosition();
    
    /*
     * Returns a heading vector to the nearest goal
     */
    virtual CVector2 VectorToGoal();
    
    virtual CVector2 GlobalVectorToGoal();
    
    virtual CVector2 LocalizeGlobalVector(CVector2 globalVector);
    
    virtual CVector2 VectorToLight();
    
    float GetChangeValue();
    
    float GetRegChangeValue();
    
    float GetOscChangeValue();

protected:

   /*
    * Calculates a random centrality value
    */
   int GetRandomCentrality();
   
  
   
   /*
    * Approximates closeness centrality using DACCER
    */
   int GetClosenessCentrality();
   
   /*
    * Gets a direction vector as input and transforms it into wheel actuation.
    */
   void SetWheelSpeedsFromVector(const CVector2& c_heading);
   
   /*
    * Converts a message into a byte sequence and pushes it to the buffer
    */
    void PushDataToBuffer(struct message messageToPush, int& pushLocation, CByteArray& cBuf);
    
   /*
    * Generates a unique ID to be used by DACCER messages
    */
    int GenerateUniqueID();
    
   /*
    * Checks if a particular message id has been received before
    * Used with DACCER
    */
    bool CheckMessageReceived(std::vector<int>& receivedMessages, int id);
    
    /*
     * Returns the centrality and ID of its most central neighbor
     */
    std::pair<int,int> GetConsensusOnLeader();
    
    /*
     * Returns the value the robots have agreed on
     */
    int GetConsensusOnValue();
    
    /*
     * Get the orientation of the robot
     */
    CRadians GetOrientation();
    
    /*
     * Returns the velocity the robots have agreed on
     */
    CVector2 GetConsensusOnGoal();
    
    /*
     * Returns the value and goal location upon which consensus is reached
     */
    void GetConsensusOnValues();
    
    /*
     * Gets consensus on goal & value when leaders are selected by centrality
     */
    void GetConsensusOnValues_centrality();
    
    /*
     * Returns a random number
     */
    int GenerateRandomNumber();
    
    /*
     * Creates a random goal location
     */
    CVector2 GenerateRandomGoal();
    
    /*
     * Returns a heading vector based on the robot's orientation
     */
    CVector2 GetHeading();
    
    /*
    * Calculates the flocking interaction vector.
    */
    virtual CVector2 FlockingVector();
    
    /* Vector pointing away from obstacle */
    virtual CVector2 ObstacleVector();
    
    /*
     * Create a message
     */
    void CreateMessage(struct message& createdMessage,bool is_RCC_message = false);
    
    
    void PrintMessage(message messageToPrint,std::string heading);

private:

   /* Pointer to the differential steering actuator */
   CCI_DifferentialSteeringActuator* m_pcWheels;
   /* Pointer to the LEDs actuator */
   CCI_LEDsActuator* m_pcLEDs;
   /* Pointer to range and bearing sensor */
   CCI_RangeAndBearingSensor* m_pcRABSens;
   /* Pointer to range and bearing actuator */
   CCI_RangeAndBearingActuator* m_pcRABAct;
   /* Pointer to position sensor */
   CCI_PositioningSensor* m_pcPosSens;
   /* Pointer to the omnidirectional camera sensor */
   CCI_ColoredBlobOmnidirectionalCameraSensor* m_pcCamera;
   /* Pointer to the foot-bot light sensor */
   CCI_FootBotLightSensor* m_pcLight;
   /* Pointer to the foot-bot proximity sensor */
   CCI_FootBotProximitySensor* m_pcProximity;

   /* The turning parameters */
   SWheelTurningParams m_sWheelTurningParams;
   /* The flocking interaction parameters. */
   SFlockingInteractionParams m_sFlockingParams;
   /* The proximity parameters */
   SProximityParams m_sProximityParams;
   /* The centrality parameters. */
   SCentralityParams m_sCentralityParams;
   

   /* Contains the messages received from neighbors */
   const CCI_RangeAndBearingSensor::SPacket* m_psNMsg;
   
   /* Flag to know whether this robot is selected */
   bool m_bSelected;

   /* The control vector */
   CVector2 m_cControl;
};

#endif

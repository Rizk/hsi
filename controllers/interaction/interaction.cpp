/* Include the controller definition */
#include "interaction.h"
/* Include uint8 and the likes */
#include <stdint.h>
/* Include vector library */
#include <vector>
/* Include pair data structure */
#include <utility>
/* Include algorithm libraries */
#include <algorithm>
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
#include <argos3/core/utility/logging/argos_log.h>
#include <math.h>

/****************************************/
/****************************************/

void CInteraction::SWheelTurningParams::Init(TConfigurationNode& t_node) {
   try {
      TurningMechanism = NO_TURN;
      CDegrees cAngle;
      GetNodeAttribute(t_node, "hard_turn_angle_threshold", cAngle);
      HardTurnOnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "soft_turn_angle_threshold", cAngle);
      SoftTurnOnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "no_turn_angle_threshold", cAngle);
      NoTurnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "max_speed", MaxSpeed);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller wheel turning parameters.", ex);
   }
}

void CInteraction::SFlockingInteractionParams::Init(TConfigurationNode& t_node) {
   try {
      GetNodeAttribute(t_node, "target_distance", TargetDistance);
      GetNodeAttribute(t_node, "gain", Gain);
      GetNodeAttribute(t_node, "exponent", Exponent);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller flocking parameters.", ex);
   }
}

void CInteraction::SProximityParams::Init(TConfigurationNode& t_node) {
   try {
	  GetNodeAttributeOrDefault(t_node, "alpha", m_cAlpha, m_cAlpha);
      m_cGoStraightAngleRange.Set(-ToRadians(m_cAlpha), ToRadians(m_cAlpha));
      GetNodeAttributeOrDefault(t_node, "delta", m_fDelta, m_fDelta);
      GetNodeAttributeOrDefault(t_node, "velocity", m_fWheelVelocity, m_fWheelVelocity);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller proximity parameters.", ex);
   }
}
 
/****************************************/
/****************************************/
 
/*
 * This function is a generalization of the Lennard-Jones potential
 */
Real CInteraction::SFlockingInteractionParams::GeneralizedLennardJones(Real f_distance) {
   Real fNormDistExp = ::pow(TargetDistance / f_distance, Exponent);
   return -Gain / f_distance * (fNormDistExp * fNormDistExp - fNormDistExp);
}

/****************************************/
/****************************************/

void CInteraction::SCentralityParams::Init(TConfigurationNode& t_node) {
   try {
      GetNodeAttribute(t_node, "centrality_measure", centralityMeasure);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller centrality parameters.", ex);
   }
}


/****************************************/
/****************************************/

CInteraction::CInteraction() :
   m_pcWheels(NULL),
   m_pcLEDs(NULL),
   m_pcLight(NULL),
   m_pcProximity(NULL),
   m_pcCamera(NULL) {}

/****************************************/
/****************************************/

void CInteraction::Init(TConfigurationNode& t_node) {
   /*
    * Get sensor/actuator handles
    *
    * The passed string (ex. "differential_steering") corresponds to the XML tag of the
    * device whose handle we want to have. For a list of allowed values, type at the
    * command prompt:
    *
    * $ argos3 -q actuators
    *
    * to have a list of all the possible actuators, or
    *
    * $ argos3 -q sensors
    *
    * to have a list of all the possible sensors.
    *
    * NOTE: ARGoS creates and initializes actuators and sensors internally, on the basis of
    *       the lists provided the configuration file at the
    *       <controllers><footbot_diffusion><actuators> and
    *       <controllers><footbot_diffusion><sensors> sections. If you forgot to
    *       list a device in the XML and then you request it here, an error occurs.
    */
   m_pcWheels = GetActuator<CCI_DifferentialSteeringActuator          >("differential_steering");
   m_pcLEDs   = GetActuator<CCI_LEDsActuator                          >("leds");
   m_pcRABAct  = GetActuator<CCI_RangeAndBearingActuator  >("range_and_bearing");
   m_pcRABSens = GetSensor  <CCI_RangeAndBearingSensor    >("range_and_bearing" );
   m_pcPosSens = GetSensor  <CCI_PositioningSensor   >("positioning" );
   m_pcCamera = GetSensor  <CCI_ColoredBlobOmnidirectionalCameraSensor>("colored_blob_omnidirectional_camera");
   m_pcLight = GetSensor <CCI_FootBotLightSensor >("footbot_light");
   m_pcProximity = GetSensor  <CCI_FootBotProximitySensor      >("footbot_proximity"    );
   
   MAX_NUM_ROBOTS = 100;//256;
   message sample;
   MESSAGE_SIZE = sizeof(sample);
   
	// A robot forwards its own message plus those of all its neighbors
   CBUF_SIZE = MAX_NUM_ROBOTS*MESSAGE_SIZE;
   
   cBuf = CByteArray(CBUF_SIZE);
   
   /* These are all parameters which can be tuned */
   TTL_VALUE = 2;
   LEADER_RESELECTION_THRESHOLD = 1000000;
   CONSENSUS_THRESHOLD = 30;
   TACIT_CONSENSUS_WEIGHT = 0.99995f;//
   TACIT_LEADERSHIP_ENABLED = true;//false;//
   SPEED_FACTOR = 0.25;
   //leaderSelectionMethod = "RANDOM";
   //leaderSelectionMethod = "DEGREE";
   //leaderSelectionMethod = "CLOSENESS";
   
   /* Initialize random number generator */
   randomNumberGenerator = CRandom::CreateRNG("argos");
   
   /* Go back to these values when resetting experiment */
   timeCounter = 0;
   valueIsStable = false;
   consensusConflict = false;
   localConsensusAchieved = true;
   leaderReselectionTimer = 0;
   consensusCounter = 0;
   centralityStored = false;
   convergedOnLeader = false;
   centralityValue = -1;
   maxCentralityValue = centralityValue;
   maxCentralityID = GetRobotID();
   valueToPass = float(GenerateRandomNumber());
   
   goalLocation = GenerateRandomGoal();
   
   swarmHeading = GlobalVectorToGoal();
   
   randomNumber = GenerateRandomNumber();
   
   //Hysteresis Parameters
   hysteresisTimer = 0;
   HYSTERESIS_MAX = 50;
   lastPosition = GetPosition();
   
   //Leader re-selection parameters
   ResetApproximateRankDrop();
   
   Deselect();
   
   
   /*
    * Parse the config file
    */
   try {
      /* Wheel turning, flocking and centrality */
      m_sWheelTurningParams.Init(GetNode(t_node, "wheel_turning"));
      m_sFlockingParams.Init(GetNode(t_node, "flocking"));
      m_sCentralityParams.Init(GetNode(t_node, "centrality"));
      m_sProximityParams.Init(GetNode(t_node, "proximity"));
      leaderSelectionMethod = m_sCentralityParams.centralityMeasure;
      m_pcCamera->Enable();
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error parsing the controller parameters.", ex);
   }
}


/****************************************/
/****************************************/

void CInteraction::ControlStep() {
	
	/*
	 LOG<<"Alpha: "<<m_sProximityParams.m_cAlpha<<std::endl;
   LOG<<"Delta: "<<m_sProximityParams.m_fDelta<<std::endl;
   LOG<<"Velocity: "<<m_sProximityParams.m_fWheelVelocity<<std::endl;
   LOG<<"AngleRange: "<<m_sProximityParams.m_cGoStraightAngleRange<<std::endl;
   
   LOG<<"Target Distance: "<<m_sFlockingParams.TargetDistance<<std::endl;
   */
	
	bufferPointer = 0;
	cBuf.Zero();
	timeCounter++;
	centralityValue = GetCentrality();
	
	//Once consensus has been reached, store the centrality
	if(consensusCounter >= CONSENSUS_THRESHOLD && !centralityStored) {
		originalCentralityValue = centralityValue;
		centralityStored = true;
		//LOG<<centralityValue<<std::endl;
	}
	
	//LOG<<"Local: "<<GetApproximateRankDrop()<<std::endl;
	GetApproximateRankDrop();
		
	if(centralityValue > maxCentralityValue && consensusCounter < CONSENSUS_THRESHOLD) {
		maxCentralityValue = centralityValue;
		maxCentralityID = GetRobotID();
	}
		
	/* Create current robot's message */
	message currentMessage;
	CreateMessage(currentMessage);
		
	PushDataToBuffer(currentMessage, bufferPointer, cBuf);
	m_pcRABAct->SetData(cBuf);
	
	maxCentralityValue = currentMessage.maxCentrality;
	maxCentralityID = currentMessage.maxID;
	
	/*
	Real positionChange = sqrt(pow(GetPosition().GetX() - lastPosition.GetX(), 2) + pow(GetPosition().GetY() - lastPosition.GetY(), 2));
	lastPosition = GetPosition();
	
	if(positionChange < 1)
		hysteresisTimer++;
	else {
		hysteresisTimer = 0;
	}
	
	if( (hysteresisTimer >= HYSTERESIS_MAX) && (GetRobotID() != maxCentralityID) ) {
		goalLocation = (90*goalLocation + 10*GenerateRandomGoal())/100;
		hysteresisTimer = 0;
	}*/
	
	/* Uncomment for goal-based flocking */
	//CVector2 robotHeading = VectorToGoal() + FlockingVector();
	
	/* Uncomment for heading-based flocking */
	CVector2 robotHeading;
	if(maxCentralityID == GetRobotID()) 
		swarmHeading = GlobalVectorToGoal();
	robotHeading = LocalizeGlobalVector(swarmHeading) + FlockingVector();// + ObstacleVector();
	
	//if(m_bSelected)
	//	robotHeading = m_cControl;
	//else
	//	robotHeading = CVector2(0.0f,0.0f);//GetFlockingVector()
		
	if(robotHeading != CVector2(0.0f,0.0f)) {// != robotSpeed(0.0f,0.0f,0.0f)
		randomNumber = GenerateRandomNumber();
	}

	/* Get readings from proximity sensor */
   const CCI_FootBotProximitySensor::TReadings& tProxReads = m_pcProximity->GetReadings();
   /* Sum them together */
   CVector2 cAccumulator;
   
   for(size_t i = 0; i < tProxReads.size(); ++i) {
      cAccumulator += CVector2(tProxReads[i].Value, tProxReads[i].Angle);
      
   }
   cAccumulator /= tProxReads.size();
   /* If the angle of the vector is small enough and the closest obstacle
    * is far enough, continue going straight, otherwise curve a little
    */
   CRadians cAngle = cAccumulator.Angle();
   if(m_sProximityParams.m_cGoStraightAngleRange.WithinMinBoundIncludedMaxBoundIncluded(cAngle) &&
      cAccumulator.Length() < m_sProximityParams.m_fDelta ) {
      /* Keep going */
      SetWheelSpeedsFromVector(robotHeading);//Uncomment for flocking;
   }
   else {
      /* Turn, depending on the sign of the angle */
      if(cAngle.GetValue() > 0.0f) {
         m_pcWheels->SetLinearVelocity(m_sProximityParams.m_fWheelVelocity, 0.0f);
      }
      else {
         m_pcWheels->SetLinearVelocity(0.0f, m_sProximityParams.m_fWheelVelocity);
      }
   
   }
	
	
}

/****************************************/
/****************************************/

void CInteraction::Reset() {
	m_pcRABAct->ClearData();
	
	timeCounter = 0;
	valueIsStable = false;
	consensusConflict = false;
	localConsensusAchieved = true;
	leaderReselectionTimer = 0;
	consensusCounter = 0;
	centralityStored = false;
	convergedOnLeader = false;
	centralityValue = -1;
	maxCentralityValue = centralityValue;
	maxCentralityID = GetRobotID();
	valueToPass = float(GenerateRandomNumber());
	
	goalLocation = GenerateRandomGoal();
	swarmHeading = GlobalVectorToGoal();
	
	randomNumber = GenerateRandomNumber();
	
	ResetApproximateRankDrop();
	
	Deselect();
}

/****************************************/
/****************************************/

int CInteraction::GetCentrality() {
	if(leaderSelectionMethod == "RANDOM")
		return GetRandomCentrality();
	if(leaderSelectionMethod == "DEGREE")
		return GetDegreeCentrality();
	if(leaderSelectionMethod == "CLOSENESS")
		return GetClosenessCentrality();
	
	return -1;
}

int CInteraction::GetRandomCentrality() {
	return randomNumber;
}

int CInteraction::GetDegreeCentrality() {
	int degree = 0;
	
	/* Get readings from neighbors (if any) */
	if(! m_pcRABSens->GetReadings().empty()) {
		degree = (m_pcRABSens->GetReadings()).size();
	}
	
	return degree;
}

/****************************************/
/****************************************/

int CInteraction::GetClosenessCentrality() {
	
	/* Centrality value that will be returned */
	int centrality = 0;
   
   /* Get readings from neighbors (if any) */
   if(! m_pcRABSens->GetReadings().empty()) {
      
      /* Create a vector of received message IDs 
       * Capacity assumes all robots get messages from all other robots
       * and pass them along to current individual
       */
      std::vector< int > receivedMessages;
      receivedMessages.reserve(MAX_NUM_ROBOTS*MAX_NUM_ROBOTS);
      
      /* For every received reading */
      for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
		  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
		  
		  /* For every message in the received reading */
		  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
			uint8_t msg_arr[MESSAGE_SIZE];
			
			for(int k=0; k<MESSAGE_SIZE; k++) {
				msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
				//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
			}
			
			/* Re-cast the received byte array as a message */
			message receivedMessage;
			memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
			
			/* If the message id is not in the list of received message ids */
			if(!CheckMessageReceived(receivedMessages, receivedMessage.id)) {
				receivedMessages.push_back(receivedMessage.id);
				
				/* If the received message has not expired and hasn't wrapped around (-1 = 255 in uint8), 
				 * put it in the buffer to be forwarded */
				
				if(receivedMessage.TTL > 0 && receivedMessage.TTL <= TTL_VALUE) {
					receivedMessage.TTL--;
					PushDataToBuffer(receivedMessage, bufferPointer, cBuf);
					centrality += receivedMessage.degree;
				}
			}
			
		}
		
	}
	
   }
   else {
      m_psNMsg = NULL;
      LOG << "none";
   }
   
   return centrality;
}

/****************************************/
/****************************************/

void CInteraction::Select() {
   m_bSelected = true;
   m_pcLEDs->SetSingleColor(12, CColor::GREEN);
}

/****************************************/
/****************************************/

void CInteraction::Deselect() {
   m_bSelected = false;
   m_pcLEDs->SetSingleColor(12, CColor::RED);
   valueIsStable = false;
}

/****************************************/
/****************************************/

void CInteraction::SetControlVector(const CVector2& c_control) {
   m_cControl = c_control;
}

/****************************************/
/****************************************/

void CInteraction::SetWheelSpeedsFromVector(const CVector2& c_heading) {
	/* Get the heading angle */
   CRadians cHeadingAngle = c_heading.Angle().SignedNormalize();
   
   /* Get the length of the heading vector */
   Real fHeadingLength = c_heading.Length();
   /* Clamp the speed so that it's not greater than MaxSpeed */
   Real fBaseAngularWheelSpeed = Min<Real>(fHeadingLength, m_sWheelTurningParams.MaxSpeed);

   /* Turning state switching conditions */
   if(Abs(cHeadingAngle) <= m_sWheelTurningParams.NoTurnAngleThreshold) {
      /* No Turn, heading angle very small */
      m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::NO_TURN;
   }
   else if(Abs(cHeadingAngle) > m_sWheelTurningParams.HardTurnOnAngleThreshold) {
      /* Hard Turn, heading angle very large */
      m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::HARD_TURN;
   }
   else if(m_sWheelTurningParams.TurningMechanism == SWheelTurningParams::NO_TURN &&
           Abs(cHeadingAngle) > m_sWheelTurningParams.SoftTurnOnAngleThreshold) {
      /* Soft Turn, heading angle in between the two cases */
      m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::SOFT_TURN;
   }

   /* Wheel speeds based on current turning state */
   Real fSpeed1, fSpeed2;
   switch(m_sWheelTurningParams.TurningMechanism) {
      case SWheelTurningParams::NO_TURN: {
         /* Just go straight */
         fSpeed1 = fBaseAngularWheelSpeed;
         fSpeed2 = fBaseAngularWheelSpeed;
         break;
      }

      case SWheelTurningParams::SOFT_TURN: {
         /* Both wheels go straight, but one is faster than the other */
         Real fSpeedFactor = (m_sWheelTurningParams.HardTurnOnAngleThreshold - Abs(cHeadingAngle)) / m_sWheelTurningParams.HardTurnOnAngleThreshold;
         fSpeed1 = fBaseAngularWheelSpeed - fBaseAngularWheelSpeed * (1.0 - fSpeedFactor);
         fSpeed2 = fBaseAngularWheelSpeed + fBaseAngularWheelSpeed * (1.0 - fSpeedFactor);
         break;
      }

      case SWheelTurningParams::HARD_TURN: {
         /* Opposite wheel speeds */
         fSpeed1 = -m_sWheelTurningParams.MaxSpeed;
         fSpeed2 =  m_sWheelTurningParams.MaxSpeed;
         break;
      }
   }

   /* Apply the calculated speeds to the appropriate wheels */
   Real fLeftWheelSpeed, fRightWheelSpeed;
   if(cHeadingAngle > CRadians::ZERO) {
      /* Turn Left */
      fLeftWheelSpeed  = fSpeed1;
      fRightWheelSpeed = fSpeed2;
   }
   else {
      /* Turn Right */
      fLeftWheelSpeed  = fSpeed2;
      fRightWheelSpeed = fSpeed1;
   }
   /* Finally, set the wheel speeds */
   m_pcWheels->SetLinearVelocity(fLeftWheelSpeed, fRightWheelSpeed);
}


/****************************************/
/****************************************/

/*------------------------------------------Helper Functions--------------------------------------------------------------------- */

int CInteraction::GetRobotID() {
	std::string idString = GetId().substr(2);
	int robotID = atoi(idString.c_str());
	return robotID;
}

void CInteraction::PushDataToBuffer(message messageToPush, int& pushLocation, CByteArray& cBuf) {
	/* Convert message struct to byte array */
	uint8_t msg_arr[sizeof(messageToPush)];
    memcpy(msg_arr, &messageToPush, sizeof(messageToPush));
    
    /* Add generated byte array to buffer */
    for(int i=0;i<sizeof(msg_arr);i++) {
	   cBuf[pushLocation+i] = msg_arr[i];
	}
	
	pushLocation += sizeof(msg_arr);
}

int CInteraction::GenerateUniqueID() {
	return randomNumberGenerator->Uniform(CRange<int>(0, INT_MAX));
}

bool CInteraction::CheckMessageReceived(std::vector<int>& receivedMessages, int id) {

	typedef std::vector<int>::iterator it_type;
	for(it_type iterator = receivedMessages.begin(); iterator != receivedMessages.end(); iterator++) {
		int savedID = *iterator;
		if(savedID == id)
			return true;
	}
	
	return false;
	
}

void CInteraction::SendValue(float value,bool tagged) {
	Select();
	valueToPass = value;
	if(tagged)
		valueIsStable = true;
}

void CInteraction::SendGoal(CVector2 goal,bool tagged) {
	//Select();
	goalLocation = goal;
	//if(tagged)
	//	valueIsStable = true;
}

float CInteraction::GetValue() {
	return valueToPass;
}

std::pair<int,int> CInteraction::GetConsensusOnLeader() {
	
	int maxCentrality = maxCentralityValue;
	int maxID = maxCentralityID;
	
	if(consensusCounter < CONSENSUS_THRESHOLD) {
		if(! m_pcRABSens->GetReadings().empty()) {
		  
		  for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
			  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
			  
			  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
				uint8_t msg_arr[MESSAGE_SIZE];
				
				for(int k=0; k<MESSAGE_SIZE; k++) {
					msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
					//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
				}
				
				message receivedMessage;
				memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
					
				if(receivedMessage.TTL == TTL_VALUE && 
				( (receivedMessage.maxCentrality > maxCentrality) || (receivedMessage.maxCentrality == maxCentrality && receivedMessage.maxID < maxID) ) ){
					
					maxCentrality = receivedMessage.maxCentrality;
					maxID = receivedMessage.maxID;
				}
				
				if(maxID == GetRobotID()) {
					Select();
					valueIsStable = true;
				}
				else {
					Deselect();
					valueIsStable = false;
				}
			}
		}
		}
		
		if(maxCentrality == maxCentralityValue && maxID == maxCentralityID)
			consensusCounter++;
		else
			consensusCounter = 0;
			
	}
	
	else {
	
		//LOG<<"CONSENSUS ";
		leaderReselectionTimer++;
		
		if(leaderReselectionTimer > LEADER_RESELECTION_THRESHOLD) {
			maxCentrality = centralityValue;
			maxID = GetRobotID();
			maxCentralityValue = maxCentrality;
			maxCentralityID = maxID;
			leaderReselectionTimer = 0;
			consensusCounter = 0;
			centralityStored = false;
		}
	}
	
	std::pair<int,int> consensusValue (maxCentrality,maxID);
	//LOG<<maxCentrality<<std::endl;
	return consensusValue;

}

int CInteraction::GenerateRandomNumber() {
	return randomNumberGenerator->Uniform(CRange<int>(0, INT_MAX));
	//return randomNumberGenerator->Uniform(CRange<int>(0, 100)); 
}

CVector2 CInteraction::GenerateRandomGoal() {
	Real xCoordinate = randomNumberGenerator->Uniform(CRange<Real>(-10, 10));
	Real yCoordinate = randomNumberGenerator->Uniform(CRange<Real>(-10, 10));
	
	return CVector2(xCoordinate,yCoordinate);
}

CVector2 CInteraction::GetPosition() {
	CVector3 reading = m_pcPosSens->GetReading().Position;
	Real xComponent = reading.GetX();
	Real yComponent = reading.GetY();
	
	return CVector2(xComponent,yComponent);
}

CRadians CInteraction::GetOrientation() {
	CRadians cXAngle;
	CRadians cYAngle;
	CRadians cZAngle;//This is the robot's orientation
	m_pcPosSens->GetReading().Orientation.ToEulerAngles(cZAngle, cYAngle, cXAngle);
	
	return cZAngle;
}

CVector2 CInteraction::GetHeading() {
	
	//LOG<<robotVelocity<<std::endl;
	Real robotOrientation = robotVelocity.GetX();
	Real robotSpeed = robotVelocity.GetY();
	
	Real actualOrientation = GetOrientation().GetValue();
	Real newOrientation;
	if(actualOrientation >= 0)
		newOrientation = -actualOrientation + robotOrientation;
	else
		newOrientation = actualOrientation + robotOrientation;
		
	/*
	if(GetRobotID() == 2) {
	LOG<<"A: "<<actualOrientation<<std::endl;
	//LOG<<"R: "<<robotOrientation<<std::endl;
	LOG<<"N: "<<newOrientation<<std::endl;
	}
	*/
	Real xComponent = cos( newOrientation );
	Real yComponent = sin( newOrientation );
	
	return robotSpeed*CVector2(xComponent,yComponent);
}

CVector2 CInteraction::VectorToGoal() {
   //This function was refined with the help of Carlo Pinciroli
   
   /* Calculate vector between me and goal in the world */
   CVector2 cMeToGoalW = goalLocation - GetPosition();
   
   /* Get my heading in the world (heading = rotation along world Z axis) */
   CRadians cMyHeadingW, cTmp1, cTmp2;
   m_pcPosSens->GetReading().Orientation.ToEulerAngles(cMyHeadingW, cTmp1, cTmp2);
   
   /* Calculate vector between me and goal with respect to my local frame of reference */
   CVector2 cMeToGoalL = cMeToGoalW.Rotate(-cMyHeadingW);
   
   if(cMeToGoalL.Length() > 0.0f) {
         cMeToGoalL.Normalize();
         cMeToGoalL *= SPEED_FACTOR*m_sWheelTurningParams.MaxSpeed;
   }
   //LOG<<"Vector is "<<cMeToGoalL<<std::endl;
   /* Return the final vector */
   return cMeToGoalL;
}

CVector2 CInteraction::GlobalVectorToGoal() {
	/* Calculate vector between me and goal in the world */
   CVector2 cMeToGoalW = goalLocation - GetPosition();
   
   return cMeToGoalW;
}

CVector2 CInteraction::LocalizeGlobalVector(CVector2 globalVector) {

	/* Get my heading in the world (heading = rotation along world Z axis) */
   CRadians cMyHeadingW, cTmp1, cTmp2;
   m_pcPosSens->GetReading().Orientation.ToEulerAngles(cMyHeadingW, cTmp1, cTmp2);
   
   /* Calculate vector between me and goal with respect to my local frame of reference */
   CVector2 cMeToGoalL = globalVector.Rotate(-cMyHeadingW);
   
   if(cMeToGoalL.Length() > 0.0f) {
         cMeToGoalL.Normalize();
         cMeToGoalL *= SPEED_FACTOR*m_sWheelTurningParams.MaxSpeed;
   }
   //LOG<<"Vector is "<<cMeToGoalL<<std::endl;
   /* Return the final vector */
   return cMeToGoalL;

}

CVector2 CInteraction::VectorToLight() {
   /* Get light readings */
   const CCI_FootBotLightSensor::TReadings& tReadings = m_pcLight->GetReadings();
   /* Calculate a normalized vector that points to the closest light */
   CVector2 cAccum;
   for(size_t i = 0; i < tReadings.size(); ++i) {
      cAccum += CVector2(tReadings[i].Value, tReadings[i].Angle);
   }
   if(cAccum.Length() > 0.0f) {
      /* Make the vector long as 1/4 of the max speed */
      cAccum.Normalize();
      cAccum *= 0.25f * m_sWheelTurningParams.MaxSpeed;
   }
   return cAccum;
}

CVector2 CInteraction::FlockingVector() {
   /* 
   if(!m_pcRABSens->GetReadings().empty()) {
      CVector2 cAccum;
      Real fLJ;
      size_t unBlobsSeen = 0;
 
      for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
 
		m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
	
         
         if(m_psNMsg->Range < m_sFlockingParams.TargetDistance * 1.80f) {
          
            fLJ = m_sFlockingParams.GeneralizedLennardJones(m_psNMsg->Range);
            
            cAccum += CVector2(fLJ,m_psNMsg->HorizontalBearing);
            
            ++unBlobsSeen;
         }
      }
      
      cAccum /= unBlobsSeen;
      
      if(cAccum.Length() > m_sWheelTurningParams.MaxSpeed) {
         cAccum.Normalize();
         cAccum *= m_sWheelTurningParams.MaxSpeed;
      }
      return cAccum;
   }
   else {
      return CVector2();
   }*/
   
   /* Get the camera readings */
   const CCI_ColoredBlobOmnidirectionalCameraSensor::SReadings& sReadings = m_pcCamera->GetReadings();
   /* Go through the camera readings to calculate the flocking interaction vector */
   if(! sReadings.BlobList.empty()) {
      CVector2 cAccum;
      Real fLJ;
      size_t unBlobsSeen = 0;

      for(size_t i = 0; i < sReadings.BlobList.size(); ++i) {

         /*
          * The camera perceives the light as a yellow blob
          * The robots have their red beacon on
          * So, consider only red blobs
          * In addition: consider only the closest neighbors, to avoid
          * attraction to the farthest ones. Taking 180% of the target
          * distance is a good rule of thumb.
          */
         if( (sReadings.BlobList[i]->Color == CColor::RED || sReadings.BlobList[i]->Color == CColor::GREEN) &&
            sReadings.BlobList[i]->Distance < m_sFlockingParams.TargetDistance * 1.80f) {
				//LOG<<sReadings.BlobList[i]->Distance<<std::endl;
				//LOG<<m_sFlockingParams.TargetDistance<<std::endl;
            /*
             * Take the blob distance and angle
             * With the distance, calculate the Lennard-Jones interaction force
             * Form a 2D vector with the interaction force and the angle
             * Sum such vector to the accumulator
             */
            /* Calculate LJ */
            fLJ = m_sFlockingParams.GeneralizedLennardJones(sReadings.BlobList[i]->Distance);
            /* Sum to accumulator */
            cAccum += CVector2(fLJ,
                               sReadings.BlobList[i]->Angle);
            /* Increment theCDegrees m_cAlpha;
   /* Maximum tolerance for the proximity reading between
    * the robot and the closest obstacle.
    * The proximity reading is 0 when nothing is detected
    * and grows exponentially to 1 when the obstacle is
    * touching the robot.
    */
   Real m_fDelta; /*blobs seen counter */
            ++unBlobsSeen;
            
         }
      }
      /* Divide the accumulator by the number of blobs seen */
      cAccum /= unBlobsSeen;
      /* Clamp the length of the vector to the max speed */
      if(cAccum.Length() > m_sWheelTurningParams.MaxSpeed) {
         cAccum.Normalize();
         cAccum *= m_sWheelTurningParams.MaxSpeed;
      }
      return cAccum;
   }
   else {
      return CVector2();
   }
}

CVector2 CInteraction::ObstacleVector() {
	/* Get readings from proximity sensor */
   const CCI_FootBotProximitySensor::TReadings& tProxReads = m_pcProximity->GetReadings();
   /* Sum them together */
   CVector2 cAccumulator;
   for(size_t i = 0; i < tProxReads.size(); ++i) {
      cAccumulator += CVector2(tProxReads[i].Value, tProxReads[i].Angle);
   }
   cAccumulator /= tProxReads.size();
   /* If the angle of the vector is small enough and the closest obstacle
    * is far enough, continue going straight, otherwise curve a little
    */
    
    return -1*cAccumulator;
   
}

int CInteraction::GetLeaderID() {
	return maxCentralityID;
}

bool CInteraction::ConvergedOnLeader() {
	
	return convergedOnLeader;
}
void CInteraction::SetLeaderSelectionMethod(std::string method) {
	leaderSelectionMethod = method;
}

int CInteraction::GetConsensusOnValue() {
	
	bool isLeader = (GetRobotID() == GetLeaderID());
	
	if(!ConvergedOnLeader())
		valueToPass = float(GenerateRandomNumber());
	
	/* Get readings from neighbors (if any) */
	if(! m_pcRABSens->GetReadings().empty()) {
      
      if(TACIT_LEADERSHIP_ENABLED && !(isLeader && ConvergedOnLeader())) {
		int numRobots = (m_pcRABSens->GetReadings()).size();
		valueToPass = TACIT_CONSENSUS_WEIGHT*valueToPass/(numRobots+1);
		}
      
      /* For every received reading */
      for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
		  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
		  
		  /* For every message in the received reading */
		  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
			uint8_t msg_arr[MESSAGE_SIZE];
			
			for(int k=0; k<MESSAGE_SIZE; k++) {
				msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
				//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
			}
			
			/* Re-cast the received byte array as a message */
			message receivedMessage;
			memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
			
			/* If the received message is from an immediate neighbor */
			if(receivedMessage.TTL == TTL_VALUE) {
				
				if(receivedMessage.isTagged && !TACIT_LEADERSHIP_ENABLED) {
					if(!(isLeader && ConvergedOnLeader())) {
						valueIsStable = true;
						valueToPass = receivedMessage.value;
						return valueToPass;
					}
				}
				
				else if(TACIT_LEADERSHIP_ENABLED) {
					if(!(isLeader && ConvergedOnLeader())) {
						valueIsStable = false;
						int numRobots = (m_pcRABSens->GetReadings()).size();
						valueToPass += TACIT_CONSENSUS_WEIGHT*receivedMessage.value/(numRobots+1);
						//LOG<<valueToPass<<std::endl;
					}
				}
			}
			}
		}
	}
	
	else {
		return valueToPass;
	}
	
	return valueToPass;
}

CVector2 CInteraction::GetConsensusOnGoal() {
	
	//LOG<<goalLocation<<std::endl;
	bool isLeader = (GetRobotID() == GetLeaderID());
	
	if(!ConvergedOnLeader())
		goalLocation = GenerateRandomGoal();
	
	/* Get readings from neighbors (if any) */
	if(! m_pcRABSens->GetReadings().empty()) {
      
      if(TACIT_LEADERSHIP_ENABLED && !(isLeader && ConvergedOnLeader())) {
		int numRobots = (m_pcRABSens->GetReadings()).size();
		goalLocation = TACIT_CONSENSUS_WEIGHT*goalLocation/(numRobots+1);
		}
      
      /* For every received reading */
      for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
		  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
		  
		  /* For every message in the received reading */
		  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
			uint8_t msg_arr[MESSAGE_SIZE];
			
			for(int k=0; k<MESSAGE_SIZE; k++) {
				msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
				//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
			}
			
			/* Re-cast the received byte array as a message */
			message receivedMessage;
			memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
			
			/* If the received message is from an immediate neighbor */
			if(receivedMessage.TTL == TTL_VALUE) {
				
				if(receivedMessage.isTagged && !TACIT_LEADERSHIP_ENABLED) {
					if(!(isLeader && ConvergedOnLeader())) {
						valueIsStable = true;
						goalLocation = receivedMessage.goal;
						return goalLocation;
					}
				}
				
				else if(TACIT_LEADERSHIP_ENABLED) {
					if(!(isLeader && ConvergedOnLeader())) {
						valueIsStable = false;
						int numRobots = (m_pcRABSens->GetReadings()).size();
						goalLocation += TACIT_CONSENSUS_WEIGHT*receivedMessage.goal/(numRobots+1);
						//LOG<<valueToPass<<std::endl;
					}
				}
			}
			}
		}
	}
	
	else {
		return goalLocation;
	}
	
	return goalLocation;
}

void CInteraction::GetConsensusOnValues() {
	//valueToPass and goalLocation are the values upon which consensus is conducted
	
	/*if(!valueIsStable) {
		goalLocation = GenerateRandomGoal();
		valueToPass = float(GenerateRandomNumber());
	}*/
	
	/* Get readings from neighbors (if any) */
	if(! m_pcRABSens->GetReadings().empty()) {
      
      if(TACIT_LEADERSHIP_ENABLED && !valueIsStable) {
		int numRobots = (m_pcRABSens->GetReadings()).size();
		goalLocation = TACIT_CONSENSUS_WEIGHT*goalLocation/(numRobots+1);
		valueToPass = TACIT_CONSENSUS_WEIGHT*valueToPass/(numRobots+1);
		swarmHeading = TACIT_CONSENSUS_WEIGHT*swarmHeading/(numRobots+1);
		}
      
      /* For every received reading */
      for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
		  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
		  
		  /* For every message in the received reading */
		  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
			uint8_t msg_arr[MESSAGE_SIZE];
			
			for(int k=0; k<MESSAGE_SIZE; k++) {
				msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
				//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
			}
			
			/* Re-cast the received byte array as a message */
			message receivedMessage;
			memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
			
			/* If the received message is from an immediate neighbor */
			if(receivedMessage.TTL == TTL_VALUE) {
				
				if(receivedMessage.isTagged && !TACIT_LEADERSHIP_ENABLED) {
					if(!valueIsStable) {
						valueIsStable = true;
						goalLocation = receivedMessage.goal;
						valueToPass = receivedMessage.value;
						swarmHeading = receivedMessage.swarmHeading;
						return;
					}
				}
				
				else if(TACIT_LEADERSHIP_ENABLED) {
					if(!valueIsStable) {
						valueIsStable = false;
						int numRobots = (m_pcRABSens->GetReadings()).size();
						goalLocation += TACIT_CONSENSUS_WEIGHT*receivedMessage.goal/(numRobots+1);
						valueToPass += TACIT_CONSENSUS_WEIGHT*receivedMessage.value/(numRobots+1);
						swarmHeading += TACIT_CONSENSUS_WEIGHT*receivedMessage.swarmHeading/(numRobots+1);
					}
				}
			}
			
			}
		}
	}
	
	else {
		return;
	}
	
	return;
}

void CInteraction::GetConsensusOnValues_centrality() {
	//valueToPass and goalLocation are the values upon which consensus is conducted
	
	bool isLeader = (GetRobotID() == GetLeaderID());
	
	if(!ConvergedOnLeader()) {
		goalLocation = GenerateRandomGoal();
		valueToPass = float(GenerateRandomNumber());
	}
	
	/* Get readings from neighbors (if any) */
	if(! m_pcRABSens->GetReadings().empty()) {
      
      if(TACIT_LEADERSHIP_ENABLED && !(isLeader && ConvergedOnLeader())) {
		int numRobots = (m_pcRABSens->GetReadings()).size();
		goalLocation = TACIT_CONSENSUS_WEIGHT*goalLocation/(numRobots+1);
		valueToPass = TACIT_CONSENSUS_WEIGHT*valueToPass/(numRobots+1);
		}
      
      /* For every received reading */
      for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
		  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
		  
		  /* For every message in the received reading */
		  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
			uint8_t msg_arr[MESSAGE_SIZE];
			
			for(int k=0; k<MESSAGE_SIZE; k++) {
				msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
				//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
			}
			
			/* Re-cast the received byte array as a message */
			message receivedMessage;
			memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
			
			/* If the received message is from an immediate neighbor */
			if(receivedMessage.TTL == TTL_VALUE) {
				
				if(receivedMessage.isTagged && !TACIT_LEADERSHIP_ENABLED) {
					if(!(isLeader && ConvergedOnLeader())) {
						valueIsStable = true;
						goalLocation = receivedMessage.goal;
						valueToPass = receivedMessage.value;
						return;
					}
				}
				
				else if(TACIT_LEADERSHIP_ENABLED) {
					if(!(isLeader && ConvergedOnLeader())) {
						valueIsStable = false;
						int numRobots = (m_pcRABSens->GetReadings()).size();
						goalLocation += TACIT_CONSENSUS_WEIGHT*receivedMessage.goal/(numRobots+1);
						valueToPass += TACIT_CONSENSUS_WEIGHT*receivedMessage.value/(numRobots+1);
						//LOG<<valueToPass<<std::endl;
					}
				}
			}
			}
		}
	}
	
	else {
		return;
	}
	
	return;
}

void CInteraction::CreateMessage(message& createdMessage, bool is_RCC_message) {
	createdMessage.id = GenerateUniqueID();
	createdMessage.TTL = TTL_VALUE;
	createdMessage.degree = (m_pcRABSens->GetReadings()).size();
	createdMessage.senderID = GetRobotID();
	std::pair<int,int> leaderConsensus = GetConsensusOnLeader();
	GetConsensusOnValues();//Sets valueToPass, goalLocation and swarmHeading
	createdMessage.value = valueToPass;//GetConsensusOnValue();//
	createdMessage.goal = goalLocation;//GetConsensusOnGoal();//
	createdMessage.swarmHeading = swarmHeading;
	createdMessage.maxCentrality = leaderConsensus.first;
	createdMessage.maxID = leaderConsensus.second;
	createdMessage.isTagged = valueIsStable;
	createdMessage.centralityChange = GetChangeValue();
}

std::vector<int> CInteraction::GetNeighbors() {
	std::vector<int> neighborRobotIDs;
	neighborRobotIDs.reserve(MAX_NUM_ROBOTS);
	//LOG<<GetRobotID()<<": ";
	
	/* Get readings from neighbors (if any) */
	if(! m_pcRABSens->GetReadings().empty()) {
      
      /* For every received reading */
      for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
		  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
		  
		  /* For every message in the received reading */
		  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
			uint8_t msg_arr[MESSAGE_SIZE];
			
			for(int k=0; k<MESSAGE_SIZE; k++) {
				msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
				//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
			}
			
			/* Re-cast the received byte array as a message */
			message receivedMessage;
			memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
			
			/* If the received message is from an immediate neighbor */
			if(receivedMessage.TTL == TTL_VALUE) {
				neighborRobotIDs.push_back(receivedMessage.senderID);
				//LOG<<receivedMessage.senderID<<" ";
			}
		}
	}
}

	//LOG<<std::endl;
	
	return neighborRobotIDs;
	
}

float CInteraction::GetApproximateRankDrop() {
	return GetRegularChange();
	//return GetOscillatoryChange();
	//return GetNeighborChange();
}

void CInteraction::ResetApproximateRankDrop() {
	ResetRegularChange();
	ResetOscillatoryChange();
}

float CInteraction::GetRegularChange() {
	regChange = abs(originalCentralityValue - centralityValue)/float(originalCentralityValue);//abs(centralityValue - oldCentralityValue);
	/*if(regChange > 1 || regChange < 0) {
		LOG<<originalCentralityValue - centralityValue<<std::endl;
		LOG<<float(originalCentralityValue)<<std::endl;
		LOG<<regChange<<std::endl;
		LOG<<"------"<<std::endl;
	}*/
	if(isnan(regChange))
		return 0;
	return regChange;
}

float CInteraction::GetOscillatoryChange() {
	if(oldCentralityValue != -1) {
		int change = centralityValue - oldCentralityValue;
		if(change > 0)
			positiveOscChange++;
		else if(change < 0)
			negativeOscChange++;
		
		//LOG<<negativeOscChange<<std::endl;
		//LOG<<positiveOscChange<<std::endl;
		
		oldCentralityValue = centralityValue;
		oscChange = ( (negativeOscChange+1.0) / (positiveOscChange+1.0) );
		return oscChange;
	}
	
	oldCentralityValue = centralityValue;
	return 1;
}

float CInteraction::GetNeighborDifference() {
	if(! m_pcRABSens->GetReadings().empty()) {
		  
		  int numberOfNeighbors = (m_pcRABSens->GetReadings()).size();
		  float accumulatedChange = 0.0;
		  float neighborChange = 0.0;
		  
		  for(int i=0; i<(m_pcRABSens->GetReadings()).size(); i++) {
			  m_psNMsg = &(m_pcRABSens->GetReadings()[i]);
			  
			  for(int j=0; j<(m_psNMsg->Data).Size(); j+= MESSAGE_SIZE) {
				uint8_t msg_arr[MESSAGE_SIZE];
				
				for(int k=0; k<MESSAGE_SIZE; k++) {
					msg_arr[k] = m_psNMsg->Data.ToCArray()[j+k];
					//TODO: This is inefficient b/c call ToCArray() each time. Call this once and store it.
				}
				
				message receivedMessage;
				memcpy(&receivedMessage	, msg_arr , sizeof(receivedMessage));
				
				if(receivedMessage.TTL == TTL_VALUE) {
					accumulatedChange += receivedMessage.centralityChange;
				}
			}
		}
		
		accumulatedChange += GetRegularChange();
		neighborChange = accumulatedChange / float(numberOfNeighbors+1);
		float neighborDifference = GetRegularChange() - neighborChange;
		
		return neighborDifference;
		
	}
	
	return 0;
}

void CInteraction::ResetRegularChange() {
	oldCentralityValue = centralityValue;
	regChange = 0;
}

void CInteraction::ResetOscillatoryChange() {
	oldCentralityValue = centralityValue;
	positiveOscChange = 0;
	negativeOscChange = 0;
	oscChange = ( (negativeOscChange+1.0) / (positiveOscChange+1.0) );
}

float CInteraction::GetChangeValue() {
	return GetRegChangeValue();
	//return GetOscChangeValue();
}

float CInteraction::GetOscChangeValue() {
	return oscChange;
}

float CInteraction::GetRegChangeValue() {
	return regChange;
}

/*------------------------------------------Testing Functions--------------------------------------------------------------------- */

void CInteraction::PrintMessage(message messageToPrint,std::string heading) {
	
	std::cout<<"-----"<<heading<<"-----"<<std::endl;
	std::cout<<"Msg ID: "<<messageToPrint.id<<std::endl;
	std::cout<<"TTL: "<<messageToPrint.TTL<<std::endl;
	std::cout<<"Msg degree: "<<messageToPrint.degree<<std::endl;
	std::cout<<"Value: "<<messageToPrint.value<<std::endl;
	std::cout<<"Sender ID: "<<messageToPrint.senderID<<std::endl;
	std::cout<<"maxCentrality: "<<messageToPrint.maxCentrality<<std::endl;
	std::cout<<"Max ID: "<<messageToPrint.maxID<<std::endl;
	std::cout<<"isTagged: "<<messageToPrint.isTagged<<std::endl;
	std::cout<<"------------------"<<std::endl;

}
/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as second argument.
 * The string is then usable in the XML configuration file to refer to this controller.
 * When ARGoS reads that string in the XML file, it knows which controller class to instantiate.
 * See also the XML configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CInteraction, "interaction_controller")

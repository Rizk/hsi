#Plots correlation between global and local rank-drop metrics and 
#calculates the correlation coefficient using Pearson product-moment 
#correlation coefficient

import numpy as np
import matplotlib.pyplot as plt
import math

def calculate_coefficient():
	f = open('Rank_Drop_Metrics.csv')
	readings = f.read().strip()

	rank_drops = [[] for i in range(20)]
	counts = [0 for i in range(20)]

	readings = readings.split('\n')

	for line in readings:
		global_metric,local_metric = line.split(',')
		global_metric = int(global_metric)
		local_metric = float(local_metric)
		if local_metric >= 0 and local_metric <= 1:
			rank_drops[global_metric] += [local_metric] #local_metric
			counts[global_metric] += 1

	#for i in range(len(rank_drops)):
	#	if counts[i] != 0:
	#		rank_drops[i] /= counts[i]
	
	#plt.boxplot(rank_drops)
	rank_drops_unaveraged = rank_drops
	rank_drops = [np.mean(rank_drops[i]) for i in range(len(rank_drops))]
	n = len(rank_drops)
	'''n_start = 0
	n_end = len(rank_drops)
	
	for i in range(len(rank_drops)):
		if math.isnan(rank_drops[i]):
			n_start = i+1
		else:
			break
			
	for i in reversed(range(len(rank_drops))):
		if math.isnan(rank_drops[i]):
			n_end = i
		else:
			break'''
	
	f, (ax1, ax2) = plt.subplots(1, 2, sharey=True) 
	#ax1.boxplot(rank_drops_unaveraged[n_start:n_end])
	ax1.boxplot(rank_drops_unaveraged)
	
	'''
	z = np.polyfit(range(n),rank_drops[n_start:n_end],1)
	print(z)
	ax2.plot(range(n),np.dot(z[0],range(n)) + z[1], color='red')
	r = np.corrcoef(range(n),rank_drops[n_start:n_end])[0][1]
	ax2.text(0.9,4,"r="+str(r))
	'''
	
	mask = np.isnan(rank_drops) | np.isinf(rank_drops)
	
	ax2.scatter(np.array(range(n))[~mask],np.array(rank_drops)[~mask])
	z = np.polyfit(np.array(range(n))[~mask],np.array(rank_drops)[~mask],1)
	ax2.plot(range(n),np.dot(z[0],range(n)) + z[1], color='red')
	r = np.corrcoef(np.array(range(n))[~mask],np.array(rank_drops)[~mask])[0][1]
	text_x = n/2.0
	text_y = np.median(np.array(rank_drops)[~mask])
	ax2.text(text_x,text_y,"r="+str(r))

	f.savefig('rank_drop_correlation.png')

if __name__ == '__main__':
    calculate_coefficient()

#calculate_coefficient()

import math
import xml.etree.ElementTree
import subprocess

rab_range = 0.6
initial_area_length = math.sqrt( ( ((rab_range - 0.1)*2)**2)/2 )
initial_num_robots = 10
max_num_robots = 910
area_per_robot = (initial_area_length**2) / initial_num_robots
uniform_increment_size = 10
intervals = range(initial_num_robots,max_num_robots,uniform_increment_size)
interval = intervals[0]
num_robots = str(interval)
new_area_length = math.sqrt(area_per_robot * interval)
spawn_area_min = '0,0,0'
#print str(new_area_length)+","+str(new_area_length)
for interval in intervals:
	new_area_length = math.sqrt(area_per_robot * interval)
	num_robots = str(interval)
print str(new_area_length)+","+str(new_area_length)

argosTree = xml.etree.ElementTree.parse('experiments/interaction.argos')
root = argosTree.getroot()
quantityElement = root.find('arena').find('distribute').find('entity')
quantityElement.set('quantity',num_robots)

positionElement = root.find('arena').find('distribute').find('position')
spawn_area_max = str(new_area_length)+","+str(new_area_length)+",0"
positionElement.set('min', spawn_area_min)
positionElement.set('max', spawn_area_max)
positionElement.set('method', 'uniform')

"""
arenaElement = root.find('arena')
x_min = [1,1,-1,-1]
y_min = [1,-1,-1,1]
x_max = [2,2,0,0]
y_max = [2,0,0,2]

for i in range(len(x_min)):

	#Create distribute parent element
	newDistributeElement = xml.etree.ElementTree.SubElement(arenaElement,'distribute')

	#Create position, orientation and entity subelements
	newPositionElement = xml.etree.ElementTree.SubElement(newDistributeElement,'position')
	newOrientationElement = xml.etree.ElementTree.SubElement(newDistributeElement,'orientation')
	newEntityElement = xml.etree.ElementTree.SubElement(newDistributeElement,'entity')

	#Set min, max and method for position
	min_xcoord = new_area_length*x_min[i]
	min_ycoord = new_area_length*y_min[i]
	min_coord = str(min_xcoord)+","+str(min_ycoord)+",0"
	max_xcoord = new_area_length*x_max[i]
	max_ycoord = new_area_length*y_max[i]
	max_coord = str(max_xcoord)+","+str(max_ycoord)+",0"
	
	newPositionElement.set('min', min_coord)
	newPositionElement.set('max', max_coord)
	newPositionElement.set('method', 'uniform')

	#Set method, mean and std_dev for orientation
	newOrientationElement.set('method', 'gaussian')
	newOrientationElement.set('mean', '0,0,0')
	newOrientationElement.set('std_dev', '360,0,0')

	#Set quantity, max_trials and base_num for entity
	newEntityElement.set('quantity',num_robots)
	newEntityElement.set('max_trials','100')
	new_base = str( interval*(i+1) )
	newEntityElement.set('base_num',new_base)

	newFootbotElement = xml.etree.ElementTree.SubElement(newEntityElement,'foot-bot')
	newFootbotElement.set('id','fb')
	newFootbotElement.set('rab_data_size','7168')
	newFootbotElement.set('rab_range',str(rab_range))
	newControllerElement = xml.etree.ElementTree.SubElement(newFootbotElement,'controller')
	newControllerElement.set('config','ic')
"""

with open('experiments/human_interaction.argos','w') as f:
	f.write(xml.etree.ElementTree.tostring(root))

process = subprocess.Popen(['argos3 -c experiments/human_interaction.argos'],shell=True)
process.wait()

#include "interaction_loop_functions.h"
#include <fstream>
/* Include vector library */
#include <vector>
#include <cmath>

void CInteractionLoopFunctions::Init(TConfigurationNode& t_tree) {

	randomNumberGenerator = CRandom::CreateRNG("argos");
	experimentID = 2;
	selectionMethod = 0;
	experimentTimer = 0;
	experimentTimerElec = 0;
	totalTimer = 0;
	TRIAL_CUTOFF = 1000000;
	MINI_CUTOFF = 990;
	valueForSending = GetRandomValue();
	goalForSending = GetRandomGoal();
	
	trialCounter = 0;
	numRobots = GetNumberOfRobots();
	MAX_ROBOTS = 25;
	INCREMENT_QUANTITY = 5;
	CONVERGENCE_ERROR_THRESHOLD = 0.05f;
	BROADCAST_START = 10;
	lastLeaderID = -1;
	GOAL_THRESHOLD = 0.2;
	numGoalsReached = 0;
	randomController = GetRandomController();
   
	Reset();
}

void CInteractionLoopFunctions::Reset() {
	//experimentTimer = 0;
}

void CInteractionLoopFunctions::PreStep() {
}

void CInteractionLoopFunctions::PostStep() {
	if(experimentID == 0) {
		if(LeaderWasChosen() && experimentTimer==0) {
			GenerateAdjacencyMatrix();
			std::cout<<"RESETTING CONTROLLERS"<<std::endl;
			ResetControllers();
		}
	}
	
	if(experimentID == -2)
		AverageDegreeExperiment();
	if(experimentID == -1)
		PreExperiment();
	if(experimentID == 1)
		Experiment1();
	if(experimentID == 2)
		Experiment2();

	totalTimer++;

}

/****************************************/
//			EXPERIMENT FUNCTIONS
/****************************************/

void CInteractionLoopFunctions::AverageDegreeExperiment() {
	
	if(experimentTimer == 1) {
		float averageDegree = 0;
		CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
		for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
			CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
			CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
			CInteraction* fbController = static_cast<CInteraction*>(&controller);
			
			averageDegree += fbController->GetDegreeCentrality();
		}
      
		averageDegree /= numRobots;
		
		std::ofstream myfile;
		myfile.open("avg_degree.txt",std::ofstream::out | std::ofstream::app);
		myfile << numRobots<<","<<averageDegree<<"\n";
		myfile.close();
	}
	
	experimentTimer++;
}

void CInteractionLoopFunctions::PreExperiment() {
	if(experimentTimer == 0) {
		/*if(!LeaderWasChosen())//assume no disconnected robots
			return;
		else {*/
			//CInteraction* leaderController = GetLeaderController();
			CInteraction* firstController = GetFirstController();
			bool tagged = true;
			valueForSending = 1337;
			firstController->SendValue(valueForSending,tagged);
			
			std::ofstream myfile;
			myfile.open("success.txt",std::ofstream::out | std::ofstream::trunc);
			myfile << "failure" <<"\n";
			myfile.close();
		//}
	}
	
	else {
		int numberOfConvergedRobots = GetNumberOfConvergedRobots();
		//LOG<<"Conv: "<<numberOfConvergedRobots<<std::endl;
		
		if(numberOfConvergedRobots == numRobots) {
			std::ofstream myfile;
			myfile.open("success.txt",std::ofstream::out | std::ofstream::trunc);
			myfile << "success"<<"\n";
			myfile.close();
		}
	}
	
	//CALCULATION FOR AVERAGE AND MAX NEIGHBORS
	if(experimentTimer == 1) {
		float averageDegree = 0;
		int maxDegree = 0;
		CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
		for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
			CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
			CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
			CInteraction* fbController = static_cast<CInteraction*>(&controller);
			
			int degree = fbController->GetDegreeCentrality();
			averageDegree += degree;
			
			if(degree > maxDegree)
				maxDegree = degree;
		}
      
		averageDegree /= numRobots;
		
		std::ofstream myfile;
		myfile.open("degree_log.txt",std::ofstream::out | std::ofstream::app);
		myfile << numRobots<<","<<averageDegree<<", "<<maxDegree<<"\n";
		myfile.close();
	}
		
		
	
	experimentTimer++;
}

void CInteractionLoopFunctions::Experiment1() {
	
	
	int leaderID = randomController->maxCentralityID;
	std::string selectionMethodName = randomController->leaderSelectionMethod;
	
	if(leaderID == lastLeaderID)
		experimentTimerElec++;
	else {
		lastLeaderID = leaderID;
		experimentTimerElec = 0;
	}
	
	//LOG<<"Leader: "<<leaderID<<std::endl;
	
	//if(experimentTimerElec > BROADCAST_START && experimentTimer == 0) {
	if(randomController->consensusCounter >= randomController->CONSENSUS_THRESHOLD && experimentTimer == 0) {
		valueForSending = GetRandomValue();//
		CInteraction* leaderController = GetLeaderController(leaderID);
		bool tagged = true;
		leaderController->SendValue(valueForSending,tagged);
		
		std::ofstream myfile;
		char buffer [4];
		sprintf(buffer,"_%d_leaders" ,numRobots);
		std::string fileName(buffer);
		//fileName = leaderSelectionMethods[selectionMethod] + fileName + ".csv";
		fileName = selectionMethodName + fileName + ".csv";
			
		myfile.open(fileName.c_str(),std::ofstream::out | std::ofstream::app);
		myfile << CountNumberOfLeaders(leaderID) <<"\n";
		myfile.close();
	}
	
	else if(experimentTimer == 0)
		return;
	
	if(experimentTimer > 0 && experimentTimer < TRIAL_CUTOFF) {
		int numberOfConvergedRobots = GetNumberOfConvergedRobots();
		
		if(numberOfConvergedRobots == numRobots) {
			std::ofstream myfile;
			char buffer [4];
			sprintf(buffer,"_%d" ,numRobots);
			std::string fileName(buffer);
			//fileName = leaderSelectionMethods[selectionMethod] + fileName + ".csv";
			fileName = selectionMethodName + fileName + ".csv";
			myfile.open(fileName.c_str(),std::ofstream::out | std::ofstream::app);
			//myfile << experimentTimer << "," << experimentTimerElec + experimentTimer -1 <<"\n";
			myfile << experimentTimer << "," << totalTimer <<"\n";
			myfile.close();
			experimentTimer = TRIAL_CUTOFF;
		}
	}
	
	else if(experimentTimer >= TRIAL_CUTOFF) {
		//LOG<<"Trial has cut off"<<std::endl;
		/*if(ChangeSelectionMethod()) {
			experimentTimer = -1;
			experimentTimerElec = 0;
			ResetControllers();
			valueForSending = GetRandomValue();
		}
		
		else {
			::exit(1);
		}*/
		
		::exit(1);
	}
	
	experimentTimer++;
}

void CInteractionLoopFunctions::Experiment2() {

	//CInteraction* randomController = GetRandomController();
	int leaderID = randomController->maxCentralityID;
	
	if(leaderID == lastLeaderID)
		experimentTimerElec++;
	else {
		lastLeaderID = leaderID;
		experimentTimerElec = 0;
		//LOG<<"Leader Reset"<<std::endl;
		experimentTimer = 0;
	}
	
	//if(experimentTimerElec > BROADCAST_START && experimentTimer == 0) {
	if(randomController->consensusCounter >= randomController->CONSENSUS_THRESHOLD && experimentTimer == 0) {
		//valueForSending = GetRandomValue();//
		CInteraction* leaderController = GetLeaderController(leaderID);
		bool tagged = true;
		leaderController->SendGoal(goalForSending,tagged);
		//LOG<<goalForSending<<std::endl;
		LOG<<"Experiment has begun"<<std::endl;
		
		/*std::ofstream myfile;
		char buffer [4];
		sprintf(buffer,"_%d_leaders" ,numRobots);
		std::string fileName(buffer);
		fileName = leaderSelectionMethods[selectionMethod] + fileName + ".csv";
			
		myfile.open(fileName.c_str(),std::ofstream::out | std::ofstream::app);
		myfile << CountNumberOfLeaders(leaderID) <<"\n";
		myfile.close();*/
	}
	
	else if(experimentTimer == 0 && totalTimer < MINI_CUTOFF)
		return;
	
	if(experimentTimer > 0 && totalTimer < MINI_CUTOFF) {
		int numberOfRobotsAtGoal = GetNumberOfRobotsAtGoal();
		//LOG<<numberOfRobotsAtGoal<<std::endl;
		//LOG<<numRobots*GOAL_THRESHOLD<<std::endl;
		//LOG<<"Global: "<<GetGlobalRankDrop(leaderID)<<std::endl;
		
		//Log metric for JUST the leader
		std::ofstream myfile;
		myfile.open("Rank_Drop_Metrics.csv",std::ofstream::out | std::ofstream::app);
		myfile<< GetGlobalRankDrop(leaderID) <<",";
		CInteraction* leaderController = GetLeaderController(leaderID);
		//myfile<<leaderController->GetChangeValue()<<"\n";
		myfile<<leaderController->GetNeighborDifference()<<"\n";
		
		//LOG<<GetGlobalRankDrop(leaderID) <<",";
		//LOG<<leaderController->GetNeighborDifference()<<std::endl;
		
		myfile.close();
		
		
		//Log metric for all robots
		/*
		std::ofstream myfile;
		myfile.open("Rank_Drop_Metrics.csv",std::ofstream::out | std::ofstream::app);
		
		CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
		for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
			CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
			CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
			CInteraction* fbController = static_cast<CInteraction*>(&controller);
			
			myfile<< GetGlobalRankDrop(leaderID) <<",";
			myfile<<fbController->GetChangeValue()<<"\n";
			
		}
		
		myfile.close();
		*/
		//myfile.close();
		
		if(numberOfRobotsAtGoal >= numRobots*GOAL_THRESHOLD) {
			numGoalsReached++;
			goalForSending = GetRandomGoal();
			CInteraction* leaderController = GetLeaderController(leaderID);
			bool tagged = true;
			leaderController->SendGoal(goalForSending,tagged);
			//LOG<<goalForSending<<std::endl;
			//LOG<<numberOfRobotsAtGoal<<std::endl;
			//LOG<<totalTimer<<std::endl;
		}
	}
	
	else if(totalTimer >= MINI_CUTOFF) {
		
		std::ofstream myfile;
		char buffer [4];
		sprintf(buffer,"%d" ,numRobots);
		std::string fileName(buffer);
		//fileName = randomController->leaderSelectionMethod + fileName + ".csv";
		fileName = fileName + ".csv";
			
		myfile.open(fileName.c_str(),std::ofstream::out | std::ofstream::app);
		myfile << numGoalsReached;// <<"\n";
		myfile.close();
		::exit(1);
	}
	
	experimentTimer++;

}

/****************************************/
//			HELPER FUNCTIONS
/****************************************/

int CInteractionLoopFunctions::GetGlobalRankDrop(int leaderID) {
	CInteraction* leaderController = GetLeaderController(leaderID);
	int leaderCentrality = leaderController->centralityValue;
	
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	int rankDrop = 0;
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      if( (fbController->centralityValue) > (leaderController->centralityValue) )
		  rankDrop++;
    }
    
    return rankDrop;
	
}

bool CInteractionLoopFunctions::LeaderWasChosen() {
	
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	CSpace::TMapPerType::iterator itToFirst = tFBMap.begin();
	CFootBotEntity* firstEntity = any_cast<CFootBotEntity*>(itToFirst->second);
    CCI_Controller& firstController = (firstEntity->GetControllableEntity()).GetController();
    CInteraction* firstFbController = static_cast<CInteraction*>(&firstController);
    int leaderID = firstFbController->GetLeaderID();
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      if(!(fbController->ConvergedOnLeader()))
		return false;
    }
    
    return true;
}

CInteraction* CInteractionLoopFunctions::GetFirstController() {
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	CSpace::TMapPerType::iterator itToFirst = tFBMap.begin();
	CFootBotEntity* firstEntity = any_cast<CFootBotEntity*>(itToFirst->second);
    CCI_Controller& firstController = (firstEntity->GetControllableEntity()).GetController();
    CInteraction* firstFbController = static_cast<CInteraction*>(&firstController);

	return firstFbController;
}

CInteraction* CInteractionLoopFunctions::GetRandomController() {
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	int randomControllerID = randomNumberGenerator->Uniform(CRange<int>(0, numRobots));
    
    for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      if(fbController->GetRobotID() == randomControllerID)
		  return fbController;
    }
}

CInteraction* CInteractionLoopFunctions::GetLeaderController(int leaderID) {
	
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	CSpace::TMapPerType::iterator itToFirst = tFBMap.begin();
	CFootBotEntity* firstEntity = any_cast<CFootBotEntity*>(itToFirst->second);
    CCI_Controller& firstController = (firstEntity->GetControllableEntity()).GetController();
    CInteraction* firstFbController = static_cast<CInteraction*>(&firstController);
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      if(fbController->GetRobotID() == leaderID)
		  return fbController;
    }
    
}

int CInteractionLoopFunctions::CountNumberOfLeaders(int leaderID) {
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	int numLeaders = 0;
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      if(fbController->GetRobotID() == leaderID)
		  numLeaders++;
    }
    
    return numLeaders;
}

int CInteractionLoopFunctions::GetNumberOfConvergedRobots() {

	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	int convergenceCounter = 0;
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      if(!fbController->TACIT_LEADERSHIP_ENABLED) {
		if(fbController->GetValue() == valueForSending)
			convergenceCounter++;
		}
		else {
			float valueDifference = fbController->GetValue() - valueForSending;
			float valueError = std::abs(valueDifference/valueForSending);
			//LOG<<valueDifference<<std::endl;
			//LOG<<valueForSending<<std::endl;
			//LOG<<valueDifference / valueForSending<<std::endl;
			//LOG<<valueError<<std::endl;
			//LOG<<"--------"<<std::endl;
			if(valueError < CONVERGENCE_ERROR_THRESHOLD)
				convergenceCounter++;
		}
	}
    
    return convergenceCounter;

}

int CInteractionLoopFunctions::GetNumberOfRobotsAtGoal() {
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	int robotCounter = 0;
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      CVector2 robotPosition = fbController->GetPosition();
      Real distanceToGoal = sqrt(pow(robotPosition.GetX() - goalForSending.GetX(), 2) + pow(robotPosition.GetY() - goalForSending.GetY(), 2));
      
      /*
      LOG<<"*****"<<std::endl;
      LOG<<distanceToGoal<<std::endl;
      LOG<<robotPosition<<std::endl;
      LOG<<goalForSending<<std::endl;
      LOG<<"-----"<<std::endl;
      */
      
      if(distanceToGoal < 2) {
		robotCounter++;
		
		}
	}
	
	//LOG<<"Goalbots: "<<robotCounter<<std::endl;
	
	return robotCounter;
}

bool CInteractionLoopFunctions::ChangeSelectionMethod() {
	selectionMethod++;
	
	if(selectionMethod >= NUM_SELECTION_METHODS)
		return false;
	
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	CSpace::TMapPerType::iterator itToFirst = tFBMap.begin();
	CFootBotEntity* firstEntity = any_cast<CFootBotEntity*>(itToFirst->second);
    CCI_Controller& firstController = (firstEntity->GetControllableEntity()).GetController();
    CInteraction* firstFbController = static_cast<CInteraction*>(&firstController);
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      fbController->SetLeaderSelectionMethod(leaderSelectionMethods[selectionMethod]);
    }
	
	return true;
}

bool CInteractionLoopFunctions::AddRobots() {
	//Increase by INCREMENT_QUANTITY
	int currentRobotQuantity = GetNumberOfRobots();
	for(int i = 0; i < INCREMENT_QUANTITY; i++)
	{
	   //Create the robot
		CFootBotEntity* addedFootbot = new CFootBotEntity("fb" + ToString(currentRobotQuantity + i), "ic", CVector3(1, 1, 1));
		CRABEquippedEntity& cRAB = addedFootbot->GetRABEquippedEntity();
		
		//Change the range
		cRAB.SetRange(0.6f);
		
		//Add to the world
		AddEntity(*addedFootbot);
	 }
	 
	 /*
	  * updateEyebotList();
	   randomiseRobotPositions();
	   for(int i = 0; i < m_eyebotControllers.size(); i++)
	   {
		   m_eyebotControllers[i]->Reset();
	   }
	  */
}

void CInteractionLoopFunctions::ResetControllers() {
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      fbController->Reset();
    }
}

int CInteractionLoopFunctions::GetNumberOfRobots() {
	
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	int counter = 0;
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it)
      counter++;
    
    return counter;
}

void CInteractionLoopFunctions::GenerateAdjacencyMatrix() {
	
	int robotQuantity = GetNumberOfRobots();
	std::vector<int> adjacencyRow(robotQuantity,0);
	std::vector< std::vector<int> > adjacencyMatrix(robotQuantity,adjacencyRow);
	
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      std::vector<int> neighbors = fbController->GetNeighbors();
      int sourceID = fbController->GetRobotID();
      
      for(int i=0; i<neighbors.size(); i++) {
		  int destinationID = neighbors[i];
		  adjacencyMatrix[sourceID][destinationID] = 1;
		  adjacencyMatrix[destinationID][sourceID] = 1;
	  }
    }
    
    std::ofstream myfile;
	char buffer [4];
			
	myfile.open("swarmMatrix.adj",std::ofstream::out | std::ofstream::trunc);
	
	for(int i=0; i<robotQuantity; i++) {
		for(int j=0; j<robotQuantity; j++) {
			myfile << adjacencyMatrix[i][j];
			if(j < (robotQuantity-1)) {
				myfile << " ";
			}
			else{
				myfile << "\n";
			}
		}
	}
	
	myfile.close();
}

float CInteractionLoopFunctions::GetRandomValue() {
	return float(randomNumberGenerator->Uniform(CRange<int>(0, INT_MAX)));
}

CVector2 CInteractionLoopFunctions::GetRandomGoal() {
	Real xCoordinate = randomNumberGenerator->Uniform(CRange<Real>(-10, 10));
	Real yCoordinate = randomNumberGenerator->Uniform(CRange<Real>(-10, 10));
	
	return CVector2(xCoordinate,yCoordinate);//CVector2(2,2);//
}

void CInteractionLoopFunctions::RandomizeSwarmValues() {
	CSpace::TMapPerType& tFBMap = GetSpace().GetEntitiesByType("foot-bot");
	
	for(CSpace::TMapPerType::iterator it = tFBMap.begin(); it != tFBMap.end(); ++it) {
      CFootBotEntity* entity = any_cast<CFootBotEntity*>(it->second);
      CCI_Controller& controller = (entity->GetControllableEntity()).GetController();
      CInteraction* fbController = static_cast<CInteraction*>(&controller);
      
      float randomValue = GetRandomValue();
      bool tagged = false;
      fbController->SendValue(randomValue,tagged);
    }
}
/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(CInteractionLoopFunctions, "interaction_loop_functions")

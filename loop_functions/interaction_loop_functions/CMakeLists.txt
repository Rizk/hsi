add_library(interaction_loop_functions MODULE 
  interaction_qtuser_functions.h
  interaction_qtuser_functions.cpp
  interaction_loop_functions.h
  interaction_loop_functions.cpp)

target_link_libraries(interaction_loop_functions
  interaction
  argos3core_simulator
  argos3plugin_simulator_entities
  argos3plugin_simulator_footbot
  argos3plugin_simulator_qtopengl
  ${QT_LIBRARIES} ${GLUT_LIBRARY} ${OPENGL_LIBRARY})

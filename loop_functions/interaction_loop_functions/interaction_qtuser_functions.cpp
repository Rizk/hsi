#include "interaction_qtuser_functions.h"
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>
#include <QKeyEvent>

/****************************************/
/****************************************/

static const Real DIRECTION_VECTOR_FACTOR = 10.;

/****************************************/
/****************************************/

CInteractionQTUserFunctions::CInteractionQTUserFunctions() :
   m_pcController(NULL) {
   /* No key is pressed initially */
   m_punPressedKeys[DIRECTION_FORWARD]  = 0;
   m_punPressedKeys[DIRECTION_BACKWARD] = 0;
   m_punPressedKeys[DIRECTION_LEFT]     = 0;
   m_punPressedKeys[DIRECTION_RIGHT]    = 0;
   
   RegisterUserFunction<CInteractionQTUserFunctions,CFootBotEntity>(&CInteractionQTUserFunctions::Draw);
}

/****************************************/
/****************************************/

void CInteractionQTUserFunctions::Draw(CFootBotEntity& c_entity) {
   CFootBotEntity* footbot = any_cast<CFootBotEntity*>(&c_entity);
    CCI_Controller& firstController = (footbot->GetControllableEntity()).GetController();
    CInteraction* fbController = static_cast<CInteraction*>(&firstController);
   
   /* Disable lighting, so it does not interfere with the chosen text color */
   glDisable(GL_LIGHTING);
   /* Disable face culling to be sure the text is visible from anywhere */
   glDisable(GL_CULL_FACE);
   /* Set the text color */
   CColor cColor(CColor::RED);
   if(fbController->valueIsStable)
		cColor = CColor::GREEN;
   glColor3ub(cColor.GetRed(), cColor.GetGreen(), cColor.GetBlue());
   /* The position of the text is expressed wrt the reference point of the footbot
    * For a foot-bot, the reference point is the center of its base.
    * See also the description in
    * $ argos3 -q foot-bot
    */
   
   /*GetOpenGLWidget().renderText(0.0, 0.0, 0.3,             // position
                                c_entity.GetId().c_str()); // text*/
    
	int centrality = fbController->centralityValue;
	int value = fbController->valueToPass;
	int maxID = fbController->maxCentralityID;
	int maxCentrality = fbController->maxCentralityValue;
	int ID = fbController->GetRobotID();
	int degree = fbController->GetDegreeCentrality();
	int hysteresis = fbController->hysteresisTimer;
	
	CVector2 goalLoc = fbController->goalLocation;
	CVector2 position = fbController->GetPosition();
	CVector2 vec2goal = fbController->VectorToGoal();
	CVector2 headingVec = fbController->swarmHeading;
     
    QString cen = QString::number(centrality);
    QString val = QString::number(value);
    QString max = QString::number(maxID);
    QString maxcen = QString::number(maxCentrality);
    QString id = QString::number(ID); 
    QString deg = QString::number(degree);
    QString hys = QString::number(hysteresis);
    
    QString pos_X = QString::number(position.GetX());
	QString pos_Y = QString::number(position.GetY());
	QString pos = pos_X + "," + pos_Y;
     
    QString goal_X = QString::number(goalLoc.GetX());
	QString goal_Y = QString::number(goalLoc.GetY());
	QString goal = goal_X + "," + goal_Y;
	
	QString vec_X = QString::number(vec2goal.GetX());
	QString vec_Y = QString::number(vec2goal.GetY());
	QString vec = vec_X + "," + vec_Y;
	
	QString head_X = QString::number(headingVec.GetX());
	QString head_Y = QString::number(headingVec.GetY());
	QString head = head_X + "," + head_Y;
     
   GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,cen);    
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,val);
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,max);   
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,"(" + maxcen + " , " + max + " , " + cen + " , " + id + ")");
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,goal);
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,pos);
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,id);
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,deg);
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,"(" + goal + " \n " + pos + ")");
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,hys);
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,vec);
   //GetQTOpenGLWidget().renderText(0.0, 0.0, 0.3,head);
   
   
                                
   /* Restore face culling */
   glEnable(GL_CULL_FACE);
   /* Restore lighting */
   glEnable(GL_LIGHTING);
}

/****************************************/
/****************************************/

void CInteractionQTUserFunctions::KeyPressed(QKeyEvent* pc_event) {
   /* Make sure that a controller was set */
   if(!m_pcController) {
      GetQTOpenGLWidget().KeyPressed(pc_event);
      return;
   }
   switch(pc_event->key()) {
      case Qt::Key_I:
         /* Forwards */
         m_punPressedKeys[DIRECTION_FORWARD] = 1;
         SetDirectionFromKeyEvent();
         break;
      case Qt::Key_K:
         /* Backwards */
         m_punPressedKeys[DIRECTION_BACKWARD] = 1;
         SetDirectionFromKeyEvent();
         break;
      case Qt::Key_J:
         /* Left */
         m_punPressedKeys[DIRECTION_LEFT] = 1;
         SetDirectionFromKeyEvent();
         break;
      case Qt::Key_L:
         /* Right */
         m_punPressedKeys[DIRECTION_RIGHT] = 1;
         SetDirectionFromKeyEvent();
         break;
      default:
         /* Unknown key */
         GetQTOpenGLWidget().KeyPressed(pc_event);
         break;
   }
}

/****************************************/
/****************************************/

void CInteractionQTUserFunctions::KeyReleased(QKeyEvent* pc_event) {
   /* Make sure that a controller was set */
   if(!m_pcController) {
      GetQTOpenGLWidget().KeyReleased(pc_event);
      return;
   }
   switch(pc_event->key()) {
      case Qt::Key_I:
         /* Forwards */
         m_punPressedKeys[DIRECTION_FORWARD] = 0;
         SetDirectionFromKeyEvent();
         break;
      case Qt::Key_K:
         /* Backwards */
         m_punPressedKeys[DIRECTION_BACKWARD] = 0;
         SetDirectionFromKeyEvent();
         break;
      case Qt::Key_J:
         /* Left */
         m_punPressedKeys[DIRECTION_LEFT] = 0;
         SetDirectionFromKeyEvent();
         break;
      case Qt::Key_L:
         /* Right */
         m_punPressedKeys[DIRECTION_RIGHT] = 0;
         SetDirectionFromKeyEvent();
         break;
      default:
         /* Unknown key */
         GetQTOpenGLWidget().KeyReleased(pc_event);
         break;
   }
}

/****************************************/
/****************************************/

void CInteractionQTUserFunctions::EntitySelected(CEntity& c_entity) {
   /* Make sure the entity is a foot-bot */
   CFootBotEntity* pcFB = dynamic_cast<CFootBotEntity*>(&c_entity);
   if(!pcFB) return;
   /* It's a foot-bot; extract its controller */
   m_pcController = dynamic_cast<CInteraction*>(&pcFB->GetControllableEntity().GetController());
   /* Tell that foot-bot that it is selected */
   m_pcController->Select();
   /* Reset key press information */
   m_punPressedKeys[DIRECTION_FORWARD]  = 0;
   m_punPressedKeys[DIRECTION_BACKWARD] = 0;
   m_punPressedKeys[DIRECTION_LEFT]     = 0;
   m_punPressedKeys[DIRECTION_RIGHT]    = 0;
}

/****************************************/
/****************************************/

void CInteractionQTUserFunctions::EntityDeselected(CEntity& c_entity) {
   /* Make sure that a controller was set (should always be true...) */
   if(!m_pcController) return;
   /* Tell the foot-bot that it is deselected */
   m_pcController->Deselect();
   /* Forget the controller */
   m_pcController = NULL;
}

/****************************************/
/****************************************/

void CInteractionQTUserFunctions::SetDirectionFromKeyEvent() {
   /* Forward/backward direction factor (local robot X axis) */
   SInt32 FBDirection = 0;
   /* Left/right direction factor (local robot Y axis) */
   SInt32 LRDirection = 0;
   /* Calculate direction factor */
   if(m_punPressedKeys[DIRECTION_FORWARD])  ++FBDirection;
   if(m_punPressedKeys[DIRECTION_BACKWARD]) --FBDirection;
   if(m_punPressedKeys[DIRECTION_LEFT])     ++LRDirection;
   if(m_punPressedKeys[DIRECTION_RIGHT])    --LRDirection;
   /* Calculate direction */
   CVector2 cDir =
      DIRECTION_VECTOR_FACTOR *
      (CVector2(FBDirection, 0.0f) +
       CVector2(0.0f, LRDirection));
   /* Set direction */
   m_pcController->SetControlVector(cDir);
}

/****************************************/
/****************************************/

REGISTER_QTOPENGL_USER_FUNCTIONS(CInteractionQTUserFunctions, "interaction_qtuser_functions")

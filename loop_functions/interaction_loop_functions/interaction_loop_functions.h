#ifndef morph_LOOP_FUNCTIONS_H
#define morph_LOOP_FUNCTIONS_H

#include <argos3/core/simulator/loop_functions.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>
#include <argos3/plugins/simulator/entities/rab_equipped_entity.h>
#include <controllers/interaction/interaction.h>

/* Include vector library */
#include <vector>

using namespace argos;

class CInteractionLoopFunctions : public CLoopFunctions {
	
public:
	
	/* Random number generator */
	CRandom::CRNG* randomNumberGenerator;
	
	int experimentID;
	int experimentLength;
	int NUM_SELECTION_METHODS = 3;
	std::string leaderSelectionMethods[3] = {"RANDOM","DEGREE","CLOSENESS"};
	int selectionMethod;
	int experimentTimer;
	int experimentTimerElec;
	int TRIAL_CUTOFF;
	int MINI_CUTOFF;
	float GOAL_THRESHOLD;
	int valueForSending;
	CVector2 goalForSending;
	int trialCounter;
	float avgConvergenceTime[3] = {0.0, 0.0, 0.0};
	float totConvergenceTime[3] = {0.0, 0.0, 0.0};
	int NUM_TRIALS;
	int MAX_ROBOTS;
	int numRobots;
	int INCREMENT_QUANTITY;
	std::vector<int> adjacencyMatrix;
	float CONVERGENCE_ERROR_THRESHOLD;
	int BROADCAST_START;
	int lastLeaderID;
	int totalTimer;
	int numGoalsReached;
	CInteraction* randomController;
	
	
public:

	virtual ~CInteractionLoopFunctions() {}
	
	virtual void Init(TConfigurationNode& t_tree);
	
	virtual void Reset();
	
	virtual void PreStep();
	
	virtual void PostStep();

private:

	void AverageDegreeExperiment();
	
	void PreExperiment();
	
	void Experiment1();
	
	void Experiment2();
	
	int GetGlobalRankDrop(int leaderID);
	
	bool LeaderWasChosen();
	
	CInteraction* GetFirstController();
	
	CInteraction* GetRandomController();
	
	CInteraction* GetLeaderController(int leaderID);
	
	int CountNumberOfLeaders(int leaderID); 
	
	int GetNumberOfConvergedRobots();
	
	int GetNumberOfRobotsAtGoal();
	
	bool ChangeSelectionMethod();
	
	bool AddRobots();
	
	void ResetControllers();
	
	int GetNumberOfRobots();
	
	void GenerateAdjacencyMatrix();
	
	float GetRandomValue();
	
	CVector2 GetRandomGoal();
	
	void RandomizeSwarmValues();


};

#endif
